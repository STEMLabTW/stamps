
# Overview
STAMPS is a Spatial and Temporal Analysis and Mapping Python Suite that provides state-of-art spatiotemporal analysis algorithms. It supports the mordern spatiotemporal geostatistics - Bayesian Maximum Entropy (BME) which is a powerful tool for incorporating prior information and observations into mapping process under conditions of uncertainty. 

STAMPS is created and maintained by the [STEMLab](https://stemlab.bse.ntu.edu.tw/wordpress/) at [National Taiwan University](https://www.ntu.edu.tw/english/).

- [Overview](#overview)
- [Installation](#installation)
  - [From Binaries](#from-binaries)
  - [From Source](#from-source)
  - [Using optional dlnm dependency](#using-optional-dlnm-dependency)
- [Getting Started](#getting-started)
- [License](#license)
- [Citing STAMPS](#citing-stamps)


# Installation
STAMPS depends on the following packages:
- [numpy](https://numpy.org)
- [scipy](https://scipy.org)
- [pandas](https://pandas.pydata.org)
- [six](https://pandas.pydata.org)

Further, the other optional dependencies are recommended to be installed:
- [matplotlib](https://matplotlib.org) is required for visualization
- [nlopt](https://nlopt.readthedocs.io/en/latest/) is required for nonlinear optimization
- [rpy2](https://rpy2.github.io) is required for using R package dlnm

## From Binaries
STAMPS can be installed from pip wheels, if all dependencies can be installed as well:

    pip intall stamps

## From Source

    git clone https://gitlab.com/STEMLabTW/stamps.git
    python -m pip install -e stamps

## Using optional dlnm dependency
If you want to do advanced time series analysis with dlnm (Distributed Lag Non-Linear Models) support, install
- [R](https://www.r-project.org) version 3.4 or above
- R packages [zoo](https://cran.r-project.org/web/packages/zoo/index.html), [dlnm](https://cran.r-project.org/web/packages/dlnm/index.html) [nlme](https://cran.r-project.org/web/packages/nlme/index.html), [mgcv](https://cran.r-project.org/web/packages/mgcv/index.html)

# Getting Started
See [stamps-samples](https://gitlab.com/STEMLabTW/stamps-samples) to learn about basic usage and background information.

# License

STAMPS is released under the [GNU General Public License v3.0](LICENSE).

# Citing STAMPS
If you use STAMPS in your research, please use the following BibTeX entry.

```BibTex
  @misc{yu2018stamps,
    author =       {Hwa-Lung Yu and Shang-Chen Ku and Chieh-Han 
                    Lee and Hua-Ting Tseng and Shih-Yao Lee},
    title =        {STAMPS},
    howpublished = {\url{https://gitlab.com/STEMLabTW/stamps}},
    year =         {2018}
  }
```