# -*- coding: utf-8 -*-
"""
Created on Fri Jul 3 09:46:39 2015
@author: hdragon689

refine on Tue Oct 1 12:16:25 2019
@refiner: HuaTing
"""
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from pandas.plotting import scatter_matrix
from matplotlib.dates import DateFormatter,rrulewrapper, RRuleLocator
from pandas.plotting import register_matplotlib_converters
import seaborn as sns
register_matplotlib_converters()


def histplot(x,bins = None,show=True):
  '''
  Plot a histogram.
  Compute and draw the histogram of x.

  Syntax:
      n, bins, patches=histplot(x,bins = None,show=True)

  Input:
  x         n by 1    1D numpy array of observations
  bins      int       number of bins 

  Output:
  n         n by 1    The values of the histogram bins.
  bins      (n+1)by1  The edges of the bins.
  patches   list      Silent list of individual patches used to create the 
                      histogram or list of such list if multiple input datasets.
  '''
  n, bins,patches  = plt.hist(x, bins = bins, edgecolor="black", linewidth=1,
    color='gray', alpha=0.8)
  plt.grid(True)
  plt.xlabel('content')
  plt.ylabel('number')
  if show:
    plt.show()
  return n, bins, patches

def scattergram(c1,c2,s=8,show=True):
  '''
  Display one data pair against each other.

  Syntax:
      scattergram(c1,c2,s=2,show=True)

  Input:
  c1        n by 1    1D numpy array of first class observations
  c2        n by 1    1D numpy array of second class observations
  s         int       size of point
  '''
  plt.scatter(c1,c2,s=s)
  plt.xlabel('class1 content')
  plt.ylabel('class2 content')
  if show:
    plt.show()

def colorplot(c,z,ax=None,s=100 ,cmap='hot_r',zrange=None,colorbar=True,show=True):  
  '''
  Plot colored symbols for the values of a vector at a set of two dimensional
  coordinates. The function uses a colormap such that the color displayed at
  these coordinates is a function of the corresponding values.  
  
  Syntax:
      ax=colorplot(c,z,ax=None,cmap='hot_r',zrange=None,show=True)
  
  Input:
  c         n by 2    2D numpy array of spatial coordinates
  z         n by 1    2D numpy array of observations
  ax        ax        Optional. Axes object for colorplot. Default is None that 
                      creates new plot.
  cmap      string    The color scheme. default is the reverse of hot scheme. 
                      Popular scheme are jet, hot, gray and their reverse xxx_r
                      the complete color scheme can refer to 
                      http://matplotlib.org/examples/color/colormaps_reference.html
  zrange    list      Two scalar contains min and max of color range, [zmin, zmax]                
  colorbar  bool      Default is True to display the colorbar    
  
  '''
  if ax is None:
    ax=plt.figure().add_subplot(111)
  else:
    ax=ax
    
  if zrange is None:
    zrange = [None,None]
  plt.scatter(x=c[:,0],y=c[:,1],c=z.reshape(-1),s=s, cmap=cmap, \
                  vmin=zrange[0], vmax=zrange[1],linewidths =1,edgecolors ='black')
  if colorbar:        
    plt.colorbar(ax=ax)
  
  xmax,xmin=np.max(c[:,0]),np.min(c[:,0])
  ymax,ymin=np.max(c[:,1]),np.min(c[:,1])
  xpadding , ypadding=0.05*(xmax-xmin),0.05*(ymax-ymin)
  ax.set_xlim(xmin-xpadding,xmax+xpadding)  
  ax.set_ylim(ymin-ypadding,ymax+ypadding)
  ax.set_xlabel('x-axis')
  ax.set_ylabel('y-axis')
  if show:
    plt.show()
  return ax  

def markerplot(c,z,ax=None,symsize=1,zrange=None,show=True): 
  '''
  Plot the values of a vector at a set of two dimensional coordinates
  using symbols of varying sizes such that the size of the displayed
  symbols at these coordinates is a function of the corresponding values. 
  
  Syntax:
      ax = markerplot(c,z,ax=None,simsize=1,zrange=None,show=True)
  
  Input:
  c         n by 2    2D numpy array of spatial coordinates.
  z         n by 1    2D numpy array of observations.
  ax        ax        Optional. Axes object for markerplot. Default is None 
                      that creates new plot.
  symsize   scalar    The size scheme. The size scale for marker display. 
                      Default is 1  
  zrange    list      Two scalar contains min and max of marker size range, 
                      [zmin, zmax].
  
  '''  
  
  if ax is None:
    ax=plt.figure().add_subplot(111)
  else:
    ax=ax
  z=z.reshape(z.size,1)  
  zmax,zmin = np.max(z),np.min(z)
  z=(z-zmin)/(zmax-zmin)*20**2
  if zrange is None:
    zrange = [None,None]

  plt.scatter(x=c[:,0],y=c[:,1],s=z*symsize,vmin=zrange[0], vmax=zrange[1],alpha = 0.5)

  xmax,xmin=np.max(c[:,0]),np.min(c[:,0])
  ymax,ymin=np.max(c[:,1]),np.min(c[:,1])
  xpadding , ypadding=0.05*(xmax-xmin),0.05*(ymax-ymin)
  plt.xlim(xmin-xpadding,xmax+xpadding)  
  plt.ylim(ymin-ypadding,ymax+ypadding)
  plt.xlabel('x-axis')
  plt.ylabel('y-axis')    
  if show:    
    plt.show()
  return ax
  
def tsplot(t,z,start,ax=None,fmt='b-',plotinterval=None,Dateshowfmt = None,show=True):
  ''' 
  Plot time series data.

  Syntax:
      tsplot(t,z,start,ax=None,fmt='b-',plotinterval=None,Dateshowfmt = None,show=True)

  Input:
  t             1 by n    1D or 2D array of time in datetime or 
                          numpy.datetime64 formats
  z             1 by n    1D or 2D array of observations
  start         string    the begin date of data.
                          Input format depend on your temporal resolution.
                          temporal resolution is monthly, Input format: year/month   ex:2019-01
                          temporal resolution is daily,   Input format: year/month/day ex:2019-01-02
  ax            axes      the axes object to be plotted
  fmt           string    line format for time series plot. 
                          Details can refer to plt.plot?    
  plotinterval  int       The interval of tick you want to show on your xlabel.Unit depend on your 
                          START input format.
                          ex: If your START input format is year/month,
                          then 'plotinterval=3' means plot interval is 3 months;
                          If your START input format is year/month/day
                          then 'plotinterval=3' means plot interval is 3 days;
                          if None, there will be totally ten ticks show on xlabel
  Dateshowfmt   str       The date format you want to show the tick.
                          ex: '%Y/%m/%d' or '%Y/%m' or '%Y'

  Remark: Details of Dateshowfmt can refer to 
          https://matplotlib.org/3.1.1/api/dates_api.html#matplotlib.dates.DateFormatter    
  '''
  tzdf = pd.DataFrame(np.nan, index=np.arange(np.max(t)-np.min(t)+1), columns=['z'])
  tzdf.loc[t - np.min(t),:] = z.reshape(-1,1)
  z = tzdf.values
  t = tzdf.index

  if ax is None:  
    fig, ax = plt.subplots()
  else:
    ax=ax
  if plotinterval is None:
    plotinterval = int(len(z)/10)

  dlen = len(start.split('-'))
  if dlen==3:
    freq = 'D'
    DateFmt = '%Y/%m/%d'
    from matplotlib.dates import DAILY
    rule = rrulewrapper(DAILY, interval=plotinterval)
  elif dlen==2:
    freq = 'M'
    DateFmt = '%Y/%m'
    from matplotlib.dates import MONTHLY
    rule = rrulewrapper(MONTHLY, interval=plotinterval)
  elif dlen==1:
    freq = 'Y'
    DateFmt = '%Y'
    from matplotlib.dates import YEARLY
    rule = rrulewrapper(YEARLY, interval=plotinterval)

  if Dateshowfmt is not None:
    DateFmt = Dateshowfmt
  
  loc = RRuleLocator(rule)

  formatter = DateFormatter(DateFmt)
  dates = pd.date_range(start=start, periods=len(z), freq=freq)
  plt.plot(dates, z)

  ax.xaxis.set_major_locator(loc)
  ax.xaxis.set_major_formatter(formatter)
  ax.xaxis.set_tick_params(rotation=30, labelsize=10)
  if show:
    plt.show()

  return ax

def histscatterplot(data,columns=None,font_scale = 0.9,half=False,show=True):

  '''
  Plot pairwise relationships in a dataset.

  Syntax:
      histscatterplot(data,columns=None,font_scale = 0.9)

  Input:
  data        m by n    a 2D array with m observations and n variables. 
                        This inputcan also be a pandas.DataFrame.
  columns     list      list of the titles of variables.
  font_scale  float     the size of the font elements.
  half        bool      set True to prevent ploting repeated scatter plot 
  
  '''  
  
  if type(data) is not pd.core.frame.DataFrame:
    data = pd.DataFrame(data,columns=columns)      
  sns.set(font_scale=font_scale)
  g = sns.pairplot(data)
  if half:
    for i, j in zip(*np.triu_indices_from(g.axes, 1)):
      g.axes[i, j].set_visible(False)
  if show:
    plt.show()


  
if __name__ == "__main__":
  c=np.random.rand(30,2)
  z=np.random.rand(30,1)
  z1 = np.random.rand(30,1)
  z2 = np.random.randn(300)
  zr=[0,0.5]
  zt = 7 + 8 * z2
  df = pd.DataFrame(np.random.randn(1000, 4), columns=['A','B','C','D'])
  
  histplot(z2,bins = 30)
  scattergram(z,z1,s = 10)
  colorplot(c,z,ax=None,cmap='jet_r',zrange=zr)
  markerplot(c,z,ax=None,symsize=1,zrange=zr)
  tsplot(np.arange(len(zt)),zt,start='2019-01-01',ax=None,fmt='b-',
    plotinterval=30,Dateshowfmt='%Y/%m')
  histscatterplot(df,half=True)
  
