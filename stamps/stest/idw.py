# -*- coding: utf-8 -*-
# @Author: Chieh-Han Lee
# @Date:   2015-08-05 19:40:44
# @Last Modified by:   Chieh-Han Lee
# @Last Modified time: 2019-07-11 21:05:51
# -*- coding: utf-8 -*-
'''
Created on 2012/4/11

@author: KSJ
'''

import numpy as np

from scipy.spatial import cKDTree as KDTree
from scipy.spatial.distance import cdist as scipy_cdist

def idw_est(x, y, z, x_est, y_est ,power = 2):
    """Inverse distance weighting (IDW) method for interpolating.

    Args:
        x (ndarray): 1-D ndarray of floats with shape (n,). 
            Data point x-coordinates.
        y (ndarray): 1-D ndarray of float with shape (n,).
            Data point y-coordinates.
        z (ndarray): 1-D ndarray of float with shape (n,).
            Data point values.
        x_est (ndarray): 1-D ndarray of floats with shape (m,).
            Points' x-coordinate at which to interpolate data.
        y_est (ndarray): 1-D ndarray of floats with shape (m,).
            Points' y-coordinate at which to interpolate data.            
        power (int):Power parameter. Greater values of p assign greater influence to values 
            closest to the interpolated point.

    Returns:
        z_est (ndarray): Interpolation result of IDW.

    Examples:

        Generate some points and randomly assign values in 
        >>> x,y = np.random.random_sample((40,2)).T*100
        >>> z = np.random.random_sample((40))*10

        Creating interpolated grid
        >>> xx,yy = np.mgrid[0:100:30j, 0:100:30j]
        >>> x_est,y_est = np.vstack((xx.ravel(),yy.ravel()))

        IDW Interpolating
        >>> from stamps.stest.idw import idw_est_coord_value
        >>> z_est = idw_est_coord_value(x, y, z, x_est, y_est ,power = 2)
        
        Plotting the results
        >>> plt.figure(figsize = (4,4))
        >>> plt.pcolormesh(xx,yy,z_est.reshape(30,30),shading = 'nearest')
        >>> plt.scatter(x,y,c = z,edgecolor = 'k',s = 25)
        >>> plt.show()
    """

    x, y, z, x_est, y_est =\
    map( lambda x : np.array( x, ndmin = 2 ),
         ( x, y, z, x_est, y_est ) )
    #dist_matrix = np.linalg.norm(
    #   np.hstack((x.T - x_est, y.T - y_est)) , axis=0 ) + 10**-10
    dist_matrix =\
        np.sqrt( ( x.T - x_est ) **2 + ( y.T - y_est ) **2 ) + 10**-10
    weight_matrix = np.reciprocal( dist_matrix ** power )
    up_matrix = weight_matrix * z.T
    up_matrix = up_matrix.sum( axis = 0 ) #sum column
    down_matrix = weight_matrix.sum( axis = 0 ) #sum column
    z_est = up_matrix / down_matrix
    return z_est

def idw_est_coord_value(coord, value, coord_est, power = 2):
    """Inverse distance weighting (IDW) method for interpolating.

    Args:
        coord (ndarray): 2-D ndarray of floats with shape (n, D). 
            Data point coordinates.
        value (ndarray): ndarray of float or complex, shape (n,)
            Data point values.
        coord_est (ndarray): 2-D ndarray of floats with shape (m, D).
            Points at which to interpolate data.
        power (int):Power parameter. Greater values of p assign greater influence to values 
            closest to the interpolated point.

    Returns:
        value_est (ndarray): Interpolation result of IDW.

    Note:
        This function do the same thing as idw_est, but much faster.

    Examples:

        Generate some points and randomly assign values in 
        >>> coord = np.random.random_sample((40,2))*100
        >>> value = np.random.random_sample((40))*10

        Creating interpolated grid
        >>> xx,yy = np.mgrid[0:100:30j, 0:100:30j]
        >>> coord_est = np.vstack((xx.ravel(),yy.ravel())).T

        IDW Interpolating
        >>> from stamps.stest.idw import idw_est_coord_value
        >>> value_est = idw_est_coord_value(coord, value, coord_est, power=2)
        
        Plotting the results
        >>> plt.figure(figsize = (4,4))
        >>> plt.pcolormesh(xx,yy,value_est.reshape(30,30),shading = 'nearest')
        >>> plt.scatter(coord[:,0],coord[:,1],c = value,edgecolor = 'k',s = 25)
        >>> plt.show()
    """
    coord_matrix = scipy_cdist(coord_est, coord) #coord_est by coord
    weight_matrix = np.reciprocal(coord_matrix**power)
    # remove dupliacted localtion (Set 0 weight)
    weight_matrix[np.isinf(weight_matrix)] = 0.
    idx_nan = np.isnan(value)
    value[idx_nan] = 0. #no data
    up_matrix = weight_matrix.dot(value)
    one_matrix = np.ones(value.shape)
    one_matrix[idx_nan] = 0.
    down_matrix = weight_matrix.dot(one_matrix)
    #all z value is zero, cause 0/0 (Set weight to 1)
    down_matrix[down_matrix==0] = 1.
    value_est = up_matrix / down_matrix
    return value_est
 
def idw_kdtree(grid_s, grid_v, grid_s_est,
    nnear=10, eps=0, power=2, weights=None, leafsize=16):
    """Inverse distance weighting (IDW) method using KDtree for interpolating.

    Args:
        grid_s (ndarray): 2-D ndarray of floats with shape (n, D). 
            Data point coordinates.
        grid_v (ndarray): ndarray of float or complex, shape (n,)
            Data point values.
        grid_s_est (ndarray): 2-D ndarray of floats with shape (m, D).
            Points at which to interpolate data.
        nnear (int):The list of k-th nearest neighbors to return. f k is an integer it is 
            treated as a list of [1, ... k] (range(1, k+1)). Note that the counting starts 
            from 1.
        eps (float):nonnegative float. Return approximate nearest neighbors;the k-th returned 
            value is guaranteed to be no further than (1+eps) times the distance to 
            the real k-th nearest neighbor.
        power (int):Power parameter. Greater values of p assign greater influence to values 
            closest to the interpolated point, with the result turning into a mosaic of tiles 
            (a Voronoi diagram) with nearly constant interpolated value for large values of p
        weights (array_like): Weighted matrix.
        leafsize (int):positive integer. 
            The number of points at which the algorithm switches over to brute-force.

    Returns:
        interp (ndarray): Interpolation result of IDW.

    Examples:

        Generate some points and randomly assign values in 
        >>> grid_s = np.random.random_sample((40,2))*100
        >>> grid_v = np.random.random_sample((40))*10

        Creating interpolated grid
        >>> xx,yy = np.mgrid[0:100:30j, 0:100:30j]
        >>> grid_s_est = np.vstack((xx.ravel(),yy.ravel())).T

        IDW Interpolating
        >>> from stamps.stest.idw import idw_kdtree
        >>> interp = idw_kdtree(grid_s, grid_v, grid_s_est, nnear=10, eps=0, power=2, weights=None, leafsize=16)
        
        Plotting the results
        >>> plt.figure(figsize = (4,4))
        >>> plt.pcolormesh(xx,yy,interp.reshape(30,30),shading = 'nearest')
        >>> plt.scatter(grid_s[:,0],grid_s[:,1],c = grid_v,edgecolor = 'k',s = 25)
        >>> plt.show()
    """

    tree = KDTree(grid_s, leafsize=leafsize)

    distances, indices = tree.query(grid_s_est, k=nnear, eps=eps)
    interp = np.zeros( (len(grid_s_est),) + np.shape(grid_v[0]) )
    iternum = 0
    for dist, idx in zip(distances, indices):

        # A qucik bug-fixed while the number of observations is less than 10 (default)
        if len(grid_v) < nnear:
            dist = dist[:len(grid_v)]
            idx = idx[:len(grid_v)]

        z0 = grid_v[idx[0]]
        if nnear == 1:
            weighted_v = grid_v[idx]
        elif dist[0] < 1e-10 and ~np.isnan(z0):
            weighted_v = z0
        else:
            ix = np.where(dist==0)[0]
            if ix.size:
                dist = np.delete(dist, ix)
                idx = np.delete(idx, ix)
            ix = np.where(np.isnan(grid_v[idx]))[0]
            dist = np.delete(dist, ix)
            idx = np.delete(idx, ix)

            weight_matrix = np.reciprocal( dist ** power )
            if weights is not None:
                weight_matrix *= weights[idx]

            weight_matrix /= np.sum(weight_matrix)
            weighted_v = np.dot(weight_matrix, grid_v[idx])

        interp[iternum] = weighted_v
        iternum += 1

    return interp
