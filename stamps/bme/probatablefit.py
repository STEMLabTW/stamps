import numpy as np
import pandas as pd
from scipy.spatial.distance import cdist
from scipy.interpolate import interp1d


def probamodel2bitable(d,dmodel,Pmodel):
    '''
    % probamodel2bitable     - build a bivariate probability table from a model
    %                          (December 1, 2003)
    %
    % Build the bivariate probability table at a specified distance from
    % the set of bivariate probability tables Pmodel available at distances
    % dmodel. The probabilities are obtained using a linear interpolation method.
    %
    % SYNTAX :
    %
    % [Pd]=probamodel2bitable(d,dmodel,Pmodel);
    %
    % INPUT :
    %
    % d         scalar     distance for which the bivariate probability table
    %                      has to be computed.
    % dmodel    nd by 1    vector of values for distances for which the bivariate
    %                      probabilities between categories have been modeled. 
    % Pmodel    nc by nc   cell array, where each cell is a nd by 1 vector of bivariate
    %                      probability values between two categories at distances
    %                      specified in dmodel.
    %
    % OUTPUT :
    %
    % Pd        nc by nc   table of bivariate probabilities at distance d
    '''
    
    nc=Pmodel.shape[0];
    Pd=np.zeros((nc,nc));

    for k in range(nc):
        for l in range(nc):
            f = interp1d(dmodel, Pmodel[k,l],kind = 'linear')
            Pd[k,l]=f(d)

    return Pd

def neighbours(c0,c,Z,nmax,dmax):
    '''
    % neighbours                - radial neighbourhood selection (Jan 1,2001)
    %
    % Select a subset of coordinates and variables based on
    % their distances from the coordinate c0.
    %
    % SYNTAX :
    %
    % [csub,Zsub,dsub,nsub,index]=neighbours(c0,c,Z,nmax,dmax);
    %
    % INPUT :
    %
    % c0      1 by d   vector of coordinates, where d is the dimension
    %                  of the space
    % c       n by d   matrix of coordinates
    % Z       n by k   matrix of values, where each column corresponds to
    %                  the values of a same variable and each line corresponds
    %                  to the values of the diferent variables at the corresponding
    %                  c coordinates
    % nmax    scalar     maximum number of lines of Z that must be kept.
    % dmax    scalar     maximum Euclidian distance between c0 and c.
    %
    % OUTPUT :
    %
    % csub    m by d   matrix which is a subset of lines of the c matrix (m<=n)
    % Zsub    m by k   matrix which is a subset of lines of the Z matrix.
    % dsub    m by 1   vector of distances between csub and c0.
    % nsub    scalar   length of the dsub vector.
    % index   m by 1   vector giving the ordering of the lines in csub with
    %                  respect to the initial matrix c.
    %
    % NOTE :
    %
    % 1-In the case of space/time coordinates, dmax is a vector of length 3,
    % and the last column of c0 and c is the temporal coordinate. In this case,
    % dmax(1) is the maximum spatial distance between the coordinate in c and c0,
    % dmax(2) is the maximum temporal distance between coordinate in c and c0,
    % and dmax(3) refers to the space/time metric, such that :
    %
    % space/time distance=spatial distance+dmax(3)*temporal distance.
    % 
    % The space/time distance is used to select the nmax closest coordinates c
    % from the estimation coordinates c0.
    %
    % 2- It is possible to specify additional 1 by 1 and n by 1 index vectors,
    % taking integer values from 1 to nv. The values in the index vectors specify
    % which of the nv variable is known at each one of the corresponding coordinates.
    % The c0 and c matrices of coordinates and the index vectors are then grouped
    % together using the MATLAB cell array notation, so that c0={c0,index0} and
    % c={c,index}.
    '''
    if isinstance(dmax,np.ndarray):
        dmax = dmax.ravel().tolist()
    if isinstance(dmax,int)|isinstance(dmax,float):
        dmax = [dmax]
    if np.isinf(nmax):
        nmax = None
    
    
    c0 = c0.reshape(1,-1)
    isST = len(dmax)==3

    if isST:

        ## spatio and temporal case 
        dt = cdist(c0[:,-1].reshape(-1,1),cs[:,-1].reshape(-1,1)).ravel() ## calculate the temporal distance between c0 and c
        ds = cdist(c0,cs).ravel() ## calculate the spatial distance between c0 and c
        d = ds + dt*dmax[2] ## combine distance in space and time
        index=np.where((ds<=dmax[2])&(dt<=dmax[1]))[0] ## find distances in space & time<=dmax
        dstdf= pd.DataFrame(d.reshape(-1,1)).loc[index] ## add the idx on each distance by create the dataframe and remove the points that the distance more than dmax
        sltidx = dstdf.sort_values(0).iloc[:nmax].index## get the index of at most the closest nsmax amount of point.
    else:
        ## pure spatail case
        d = cdist(c0,c).ravel() ## calculate the distance between c0 and c
        ddf= pd.DataFrame(d.reshape(-1,1)) ## add the idx on each distance by create the dataframe
        ddf = ddf.loc[d<dmax[0]]## remove the points that the distance more than dmax
        sltidx = ddf.sort_values(0).iloc[:nmax].index## get the index of at most the closest nsmax amount of point.
    csub,Zsub,dsub,nsub,index = c[sltidx,:],Z[sltidx,:],d.ravel()[sltidx],len(sltidx),sltidx

    return csub,Zsub,dsub,nsub,index

def designmatrix(c,order):
    '''
    % designmatrix              - design matrix in a linear regression model (Jan 1,2001)
    %
    % Build the design matrix associated with a polynomial
    % mean of a given order in a linear regression model
    % of the form z=X*b+e.
    %
    % SYNTAX :
    %
    % [X,index]=designmatrix(c,order);
    %
    % INPUT :
    %
    % c       n by d       matrix of coordinates for the locations. A line
    %                      corresponds to the vector of coordinates at a
    %                      location, so the number of columns in c corresponds
    %                      to the dimension of the space. There is no restriction
    %                      on the dimension of the space.
    % order   scalar       order of the polynomial mean along the spatial axes
    %                      specified in c, where order>=0. When order=NaN, an empty
    %                      X matrix is returned.
    %
    % OUTPUT :
    %
    % X       n by k       design matrix, where each column corresponds to one
    %                      of the polynomial term, sorted in the first place 
    %                      with respect to the degree of the polynomial term,
    %                      and sorted in the second place with respect to the 
    %                      axis number. 
    %
    % index   1 or 2 by k  matrix associated with the columns of X. The first line
    %                      specifies the degree of the estimated polynomial term for
    %                      the corresponding column of X, and the second line specifies
    %                      the axis number to which this polynomial term belongs. The
    %                      axis are numbered according to the columns of c. E.g., the axis
    %                      2 corresponds to the second column of c. Note that the value 0
    %                      in the second line of index is associated with the polynomial
    %                      term of degree equal to 0 (i.e., the constant term) that is 
    %                      defined jointly for all the axes. In the singular case where c
    %                      is a column vector (i.e., the dimension of the space is equal
    %                      to 1), there is only one line for the index variable.
    %
    % NOTE :
    %
    % 1- It is also possible to process several variables at the same time
    % (multivariate case). It is needed to specify additionally tags in the
    % c matrix. These tags are provided as a vector of values that refers to
    % the variable, the values ranging from 1 to nv, where nv is the number
    % of variables. E.g., if there are 3 variables, the input index column vector
    % must be defined, and the elements in index are equal to 1, 2 or 3. The
    % c and index variables are grouped using the MATLAB cell array notation,
    % so that c={c, index}, is now the correct input variable. Using the same
    % logic, order is now a column vector specifying the order of the polynomial
    % mean for each variable. For the output variable index, there is an additional
    % first column that refers to the variable number associated with the
    % corresponding column of X.
    %
    % 2- For space/time data, the convention is that the last column of the c
    % matrix of coordinates corresponds to the time axis. Is is then possible to
    % specify a different order for the polynomial along the spatial axes and the
    % temporal axis. For the univariate case, order is a 1 by 2 vector, where
    % order(1) is the order of the spatial polynomial and order(2) is the order of
    % the temporal polynomial. For the multivariate case where nv different variables
    % are considered, order is a nv by 2 matrix, where the first and second columns
    % of order contain the order of the spatial and the temporal polynomial for
    % each of the nv variables, respectively. If in that case order is entered as
    % a 1 by 2 matrix, the same spatial order corresponding to order(1) will be used
    % for all the variables.
    '''
    n,nd=c.shape
    X =np.array([]).reshape(n,0)
    index = np.array([]).reshape(2,0)
    
    order=[order,order]
    if ~(np.isnan(order[0]) & np.isnan(order[1])):
        X=np.hstack((X,np.ones((n,1))))
        index=np.hstack((index,np.array([0,0]).reshape(-1,1)))
    if ~np.isnan(order[0]):
        for j in range(1,order[1]+1):
            for k in range(1,nd):
                X=np.hstack((X,c[:,[k]]**j))
                index=np.hstack((index,np.array([j,k]).reshape(-1,1)))
    if ~np.isnan(order[1]):
        for j in range(1,order[1]+1):
            X=np.hstack((X,c[:,[nd-1]]**j))
            index=np.hstack((index,np.array([j,nd]).reshape(-1,1)))
    if ((index.size!=0)&(nd==1)):
        index=index[[0],:]

    return X,index

def gausspdf(z,param):
    '''
    % gausspdf                  - Gaussian probability distribution function (Jan 1,2001)
    %
    % Compute the values of the probability distribution
    % function for a Gaussian distribution with
    % specified mean and variance parameters.
    %
    % SYNTAX :
    %
    % [pdf]=gausspdf(z,param);
    %
    % INPUT :
    %
    % z        n by k   matrix of values for which the probability
    %                   distribution function must be computed.
    % param    1 by 2   parameters of the Gaussian distribution, where :
    %                   param(1) is the mean of the distribution,
    %                   param(2) is the variance of the distribution.
    %
    % OUTPUT :
    %
    % pdf      n by k   matrix of values for the probability distribution
    %                   function computed at the corresponding z values.
    '''
    
    m,v=param

    if v<0:
        raise SyntaxError('a variance cannot be negative')

    if v!=0:
        A=1/np.sqrt(2*np.pi*v)
        pdf=A*np.exp(-0.5*((z-m)/np.sqrt(v))**2)
    else:
        pdf=np.zeros(z.shape)
        pdf=np.where(z==m,np.inf,pdf)
    return pdf

def regression(c,z,order,K= None):
    '''
    % regression                - parameters estimation in a linear regression model (Jan 1,2001)
    %
    % Standard implementation of the least squares estimation
    % procedure in a linear regression model, where the
    % deterministic part of the linear model is a polynomial
    % of arbitrary order. Though it is presented here in a
    % spatial context, it can be used for a wide variety of
    % other non spatial cases. 
    %
    % SYNTAX :
    %
    % [best,Vbest,zest,index]=regression(c,z,order,K); 
    %
    % INPUT :
    %
    % c        n by d       matrix of coordinates for the locations. A line
    %                       corresponds to the vector of coordinates at a
    %                       location, so the number of columns in c corresponds
    %                       to the dimension of the space. There is no restriction
    %                       on the dimension of the space.
    % z        n by 1       vector of values at the c coordinates.
    % order    scalar       order of the polynomial mean along the spatial axes
    %                       specified in c, where order>=0.
    % K        n by n       optional square symmetric matrix of covariance for the
    %                       values specified in z. When K is specified, the generalized
    %                       least squares parameter estimates are computed, whereas the
    %                       ordinary least squares parameter estimates are computed
    %                       otherwise.
    %
    % OUTPUT :
    %
    % best     k by 1       vector of parameter estimates.
    % Vbest    k by k       square symmetric matrix  of covariance for the best parameter
    %                       estimates.
    % zest     n by 1       vector of estimated regression values at the c coordinates.
    % index    k by 1 or 2  matrix associated with best. The first column specifies the
    %                       degree of the estimated polynomial term for the corresponding
    %                       best element, and the second column specifies the axis number
    %                       to which this polynomial term belongs. The axis are numbered
    %                       according to the columns of c. E.g., the axis 2 corresponds to
    %                       the second column of c. Note that the value 0 in the second
    %                       column of index is associated with the polynomial term of degree
    %                       equal to 0 (i.e., the constant term) that is defined jointly for
    %                       all the axes. In the singular case where c is a column vector
    %                       (i.e., the dimension of the space is equal to 1), there is only
    %                       one column for the index variable.
    %
    % NOTE :
    %
    % 1- It is also possible to process several variables at the same time
    % (multivariate case). It is needed to specify additionally tags in the
    % c matrix. These tags are provided as a vector of values that refers to
    % the variable, the values ranging from 1 to nv, where nv is the number
    % of variables. E.g., if there are 3 variables, the input index column vector
    % must be defined, and the elements in index are equal to 1, 2 or 3. The
    % c and index variables are grouped using the MATLAB cell array notation,
    % so that c={c,index}, is now the correct input variable. Using the same
    % logic, order is now a column vector specifying the order of the polynomial
    % mean for each variable. For the output variable index, there is an additional
    % first column that refers to the variable number associated with the
    % corresponding best elements. If regression.m is used for processing several
    % variables at the same time and if K is not specified, the results are the
    % same than those obtained when using the function separately for each variable.
    % This is because there are no interactions taken into account between the
    % variables in the ordinary least squares case.
    %
    % 2- For space/time data, the convention is that the last column of the c
    % matrix of coordinates corresponds to the time axis. Is is then possible to
    % specify a different order for the polynomial along the spatial axes and the
    % temporal axis. For the univariate case, order is a 1 by 2 vector, where
    % order(1) is the order of the spatial polynomial and order(2) is the order of
    % the temporal polynomial. For the multivariate case where nv different variables
    % are considered, order is a nv by 2 matrix, where the first and second columns
    % of order contain the order of the spatial and the temporal polynomial for
    % each of the nv variables, respectively. If in that case order is entered as
    % a 1 by 2 matrix, the same spatial order corresponding to order(1) and the same
    % temporal order corresponding to order(2) will be used for all the variables.
    '''
    from numpy.linalg import inv

    X,index=designmatrix(c.reshape(-1,1),order)
    index=index.T
    n,p=X.shape
    Xt=X.T
    z = z.reshape(-1,1)
    if X.size !=0:
        if K is None:
            invXtX=inv(np.dot(Xt,X))
            best=np.dot(np.dot(invXtX,Xt),z)
            zest=np.dot(X,best)
            resi=z-zest
            s2=(np.dot(resi.T,resi))/(n-p)
            Vbest=invXtX*s2
        else:
            XtinvK=np.dot(X.T,inv(K))
            invXtinvKX=inv(np.dot(XtinvK,X))
            best=np.dot(np.dot(invXtinvKX,XtinvK),z)
            zest=np.dot(X,best)
            Vbest=invXtinvKX
    
    return best,Vbest,zest,index

def smooth(dk,d,p,o,kstd,order,weighting):
    '''
    % smooth                 - smoothing using a Gaussian kernel regression method
    %                          (December 1,2003)
    %
    % Implementation of the regression.m function in a moving
    % neighbourhood context. Regression is conducted locally
    % at a set of coordinates using a least squares estimation
    % procedure for a linear regression model, where the
    % deterministic part of the linear model is a polynomial of
    % a given order. Instead of using ordinary least squares,
    % the function uses a diagonal covariance matrix, where the
    % variances are inversely proportional to the weights provided
    % by a Gaussian kernel, as well as inversely proportional to
    % the number of observations. 
    %
    % SYNTAX :
    %
    % [pk]=smooth(dk,d,p,o,kstd,order,weighting); 
    %
    % INPUT :
    %
    % dk          nk by 1   vector of distances for the smoothed estimates.
    % d           n by 1    vector of distances for which the bivariate 
    %                       probabilities have been estimated.
    % p           n by 1    vector of probabilities at distances specified in d.
    % o           n by 1    vector of number of pairs separated by distances
    %                       specified in d. 
    % kstd        scalar    standard deviation of the Gaussian kernel function
    %                       which is used in the kernel regression smoothing
    % order       scalar    order of the polynomial used in the kernel regression
    %                       smoothing
    % weighting   scalar    weigthing is equal to 1 or 0, depending if the user wants
    %                       or does not want to use the number of observations
    %                       as weights.
    %
    % OUTPUT :
    %
    % pk          nk by 1   vector of estimated probabilities at distances dk.
    '''

    nk = len(dk)
    pk=np.zeros((nk,1))*np.nan
    nmax=np.inf 

    for i in range(nk):
        dmaxi=4*kstd
        dsub,posub,trash,nsub,_ = neighbours(dk[i],d,np.hstack((p,o)),nmax,dmaxi)
        if nsub>0:
            dsub=dsub-dk[i]
            w=gausspdf(dsub,[0,kstd**2])
            w = 1/w
            if weighting==1:
                w=w*posub[:,[1]]
            K=np.diag(w.ravel())
            best,Vbest,zest,index=regression(dsub,posub[:,[0]],order,K)
            pk[i]=best[0]
        else:
            print(f'Warning: No estimation at distance {dk(i)} - increase smoothing parameter')
    return pk

def probatablefit(dfit,d,P,o,kstd,options = [0,0,1]):
    
    '''
    % probatablefit          - fitting of the multicategory bivariate probability tables
    %                          (December 1,2003)
    %
    % SYNTAX : [Pfit]=probatablefit(dfit,d,P,o,kstd,options);
    %
    % INPUT :
    %
    % dfit      n by 1       vector of values that specify the distances for which fitted
    %                        bivariate probability values are sought. Maximum distance in
    %                        dfit cannot exceed maximum distance in d.
    % d         ncl by 1     vector giving the sorted values of the mean distance separating
    %                        the pairs of points that belong to the same distance class. The
    %                        first value corresponds to a null distance. 
    % P         nc by nc     symmetric array of cells that contains the bivariate probability
    %                        tables estimates between the nc categories for the distance classes
    %                        specified in d. Diagonal cells contain the ncl by 1 vector of
    %                        probability estimates for the same category, whereas off-diagonal
    %                        cells contain the ncl by 1 vector of cross-category probability
    %                        estimates.For diagonal cells, the first value of each vector is
    %                        the estimated proportion of the corresponding category, whereas
    %                        this first value is always equal to 0 for off-diagonal cells.
    % o         ncl by 1     vector giving the number of pairs of points that belong to the 
    %                        corresponding distance classes. 
    % kstd      scalar       standard deviation of the Gaussian kernel function which is used
    %                        in the kernel regression smoothing (the higher the value is, the
    %                        stronger the smoothing will be). Suggested starting value is
    %                        about 0.5*max(d)/length(d).
    % options   1 by 3       optional vector of parameters that can be used if default values are
    %                        not satisfactory (otherwise this vector can simply be omitted from the
    %                        input list of variables), where :
    %                        options(1) displays the fitted probability tables if the value is set
    %                        to one (default value is 0).
    %                        options(2) is the order of the polynomial used in the kernel regression
    %                        smoothing (default value is 0).
    %                        options(3) uses the number of obervations as weights for the fitting
    %                        if the value is set to one (default value is 1).
    %
    % OUTPUT :
    %
    % Pfit      nc by nc     cell array that contains the fitted bivariate probability tables
    %                        between the nc categories for the distance classes specified in dfit.
    %                        Diagonal cells contain the n by 1 vector of probability estimates
    %                        for the same category, whereas off-diagonal cells contain the n by
    %                        1 vector of cross-category probability estimates.
    '''
    
    
    if np.max(dfit)>np.max(d):
        raise SyntaxError('maximum distance in dfit cannot exceed maximum distance in d')

    ### Compute table of bivariate probabilities at null distance

    P0=probamodel2bitable(0,d.ravel(),P)

    ### Create symmetric vectors for distance and number of 
    ### observations around null distance

    nv= P.shape[0]
    nd= d.shape[0]
    dsym=np.vstack((np.flipud(-d),d[1:nd]))
    osym=np.vstack((np.flipud(o),o[1:nd]))


    ### Interpolate bivariate probabilities at dfit using a kernel 
    ### regression method. Values are mirrored first around null distance ;
    ### this makes possible to avoid the border effect when smoothing for
    ### dfit values close to zero
    Pfit = []
    for i in range(nv):
        Pfit.append([])
        for j in range(nv):
            Pfit[i].append([]) # corresponding to matlab code Pfit=cell(nv,nv); create nv,nv list 
    for i in range(nv):
        for j in range(nv): 
            psym=np.hstack((np.flipud(-P[i,j,:])+2*P0[i,j],P[i,j,:][1:nd])).reshape(-1,1)
            Pfit[i][j]=smooth(dfit,dsym,psym,osym,kstd,options[1],options[2])
            Pfit[j][i]=Pfit[i][j]
            minPfitij=np.min(Pfit[i][j])
            if (minPfitij<0)&(minPfitij>-np.spacing(1)):
                index=(Pfit[i][j]<0)
                Pfit[i][j][index]=0
            if max(Pfit[i][j])<0:
                print(f'Warning: Non valid model - probabilities below 0 for categories {i}-{j}')
                print('Try to change polynomial order or smoothing parameter')
            if max(Pfit[i][j])>1:
                print(f'Warning: Non valid model - probabilities above 1 for categories {i}-{j}')
                print('Try to change polynomial order or smoothing parameter');
            if np.isnan(sum(Pfit[i][j])):
                print(f'Warning : some fitted values are NaN\'s for categories {i}-{j}')
                print('Increase smoothing parameter or use other distance classes in probatablecalc.m')
    Pfit = np.array(Pfit)[:,:,:,0]
    return Pfit