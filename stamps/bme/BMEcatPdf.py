import numpy as np
import pandas as pd
from scipy.interpolate import interp1d
from scipy.spatial.distance import cdist
def coord2dist( c1, c2, norm=2 ):
    '''
    Calculate the distance between coordinates c1 and c2
    
    Syntax: result = coord2dist(c1, c2)

    Input 
        c1        [r1 x d]     np.array of coordinates
        c2        [r2 x d]     np.array of coordinates
    Output
        d         [r1 x r2]    np.array of distances
    '''

    try:
        d = cdist(c1, c2, 'minkowski', p=norm)  
    except:
        ones_c1 = np.ones((c1.shape[0], 1))
        ones_c2 = np.ones((c2.shape[0], 1))
        a = np.kron(c1, ones_c2)
        b = np.kron(ones_c1, c2)
        d = ((a - b) ** 2)
        d = d.sum(axis=1)
        d = np.sqrt(d.astype(float))
        d = d.reshape((c1.shape[0], c2.shape[0]))
    return d

def neighbours(c0,c,Z,nmax,dmax):
    '''
    % neighbours                - radial neighbourhood selection (Jan 1,2001)
    %
    % Select a subset of coordinates and variables based on
    % their distances from the coordinate c0.
    %
    % SYNTAX :
    %
    % [csub,Zsub,dsub,nsub,index]=neighbours(c0,c,Z,nmax,dmax);
    %
    % INPUT :
    %
    % c0      1 by d   vector of coordinates, where d is the dimension
    %                  of the space
    % c       n by d   matrix of coordinates
    % Z       n by k   matrix of values, where each column corresponds to
    %                  the values of a same variable and each line corresponds
    %                  to the values of the diferent variables at the corresponding
    %                  c coordinates
    % nmax    scalar     maximum number of lines of Z that must be kept.
    % dmax    scalar     maximum Euclidian distance between c0 and c.
    %
    % OUTPUT :
    %
    % csub    m by d   matrix which is a subset of lines of the c matrix (m<=n)
    % Zsub    m by k   matrix which is a subset of lines of the Z matrix.
    % dsub    m by 1   vector of distances between csub and c0.
    % nsub    scalar   length of the dsub vector.
    % index   m by 1   vector giving the ordering of the lines in csub with
    %                  respect to the initial matrix c.
    %
    % NOTE :
    %
    % 1-In the case of space/time coordinates, dmax is a vector of length 3,
    % and the last column of c0 and c is the temporal coordinate. In this case,
    % dmax(1) is the maximum spatial distance between the coordinate in c and c0,
    % dmax(2) is the maximum temporal distance between coordinate in c and c0,
    % and dmax(3) refers to the space/time metric, such that :
    %
    % space/time distance=spatial distance+dmax(3)*temporal distance.
    % 
    % The space/time distance is used to select the nmax closest coordinates c
    % from the estimation coordinates c0.
    %
    % 2- It is possible to specify additional 1 by 1 and n by 1 index vectors,
    % taking integer values from 1 to nv. The values in the index vectors specify
    % which of the nv variable is known at each one of the corresponding coordinates.
    % The c0 and c matrices of coordinates and the index vectors are then grouped
    % together using the MATLAB cell array notation, so that c0={c0,index0} and
    % c={c,index}.
    '''
    if isinstance(dmax,np.ndarray):
        dmax = dmax.ravel().tolist()
    if isinstance(dmax,int)|isinstance(dmax,float):
        dmax = [dmax]
    if np.isinf(nmax):
        nmax = None
    
    
    c0 = c0.reshape(1,-1)
    isST = len(dmax)==3

    if isST:

        ## spatio and temporal case 
        dt = cdist(c0[:,-1].reshape(-1,1),cs[:,-1].reshape(-1,1)).ravel() ## calculate the temporal distance between c0 and c
        ds = cdist(c0,cs).ravel() ## calculate the spatial distance between c0 and c
        d = ds + dt*dmax[2] ## combine distance in space and time
        index=np.where((ds<=dmax[2])&(dt<=dmax[1]))[0] ## find distances in space & time<=dmax
        dstdf= pd.DataFrame(d.reshape(-1,1)).loc[index] ## add the idx on each distance by create the dataframe and remove the points that the distance more than dmax
        sltidx = dstdf.sort_values(0).iloc[:nmax].index## get the index of at most the closest nsmax amount of point.
    else:
        ## pure spatail case
        d = cdist(c0,c).ravel() ## calculate the distance between c0 and c
        ddf= pd.DataFrame(d.reshape(-1,1)) ## add the idx on each distance by create the dataframe
        ddf = ddf.loc[d<dmax[0]]## remove the points that the distance more than dmax
        sltidx = ddf.sort_values(0).iloc[:nmax].index## get the index of at most the closest nsmax amount of point.
    csub,Zsub,dsub,nsub,index = c[sltidx,:],Z[sltidx,:],d.ravel()[sltidx],len(sltidx),sltidx

    return csub,Zsub,dsub,nsub,index


def probamodel2bitable(d,dmodel,Pmodel):
    '''
    % probamodel2bitable     - build a bivariate probability table from a model
    %                          (December 1, 2003)
    %
    % Build the bivariate probability table at a specified distance from
    % the set of bivariate probability tables Pmodel available at distances
    % dmodel. The probabilities are obtained using a linear interpolation method.
    %
    % SYNTAX :
    %
    % [Pd]=probamodel2bitable(d,dmodel,Pmodel);
    %
    % INPUT :
    %
    % d         scalar     distance for which the bivariate probability table
    %                      has to be computed.
    % dmodel    nd by 1    vector of values for distances for which the bivariate
    %                      probabilities between categories have been modeled. 
    % Pmodel    nc by nc   cell array, where each cell is a nd by 1 vector of bivariate
    %                      probability values between two categories at distances
    %                      specified in dmodel.
    %
    % OUTPUT :
    %
    % Pd        nc by nc   table of bivariate probabilities at distance d
    '''
    
    nc=Pmodel.shape[0]
    Pmodel_vec = Pmodel.reshape(nc**2,-1)

    ## find the distance in dmodel
    loc = np.interp(d,dmodel,range(len(dmodel)))
    
    ## do interpolation in Pmodel
    idx = int(loc//1)
    Pd = (Pmodel_vec[:,idx]*(1-loc%1) + Pmodel_vec[:,idx+1]*(loc%1)).reshape(nc,nc)

    return Pd

def sumoverallexcepttwo(P,dim1,dim2):
    '''

    % sumoverallexcepttwo    - sum along all dimensions of a multidimensional table except two
    %                          (December 1, 2003)
    %
    % Sum the values of a multidimensional table along all of its dimensions
    % except two of them.
    %
    % SYNTAX : 
    %
    % [S]=sumoverallexcepttwo(P,dim1,dim2);
    %
    % INPUT :
    %
    % P         nc by ... by nc   ndim-dimensional table with nc elements along
    %                             each one of the ndim dimensions.
    % dim1      scalar            first dimension along which the summation does
    %                             not operate.
    % dim2      scalar            second dimension along which the summation does
    %                             not operate.
    %
    % OUTPUT :
    %
    % S         nc by nc          two-dimensional table obtained by summation over P
    %                             except over dimensions dim1 and dim2.
    '''
    ndim=P.ndim

    sumaxis = tuple(np.delete(np.arange(ndim),[dim1,dim2]))
    S = P.sum(axis = sumaxis, keepdims=True)
    return S

def multiplysubtable(P,dim,index,ptheor,pest):
    '''
    % multiplysubtable       - multiplication of a subtable by a ratio of scalars
    %                          (December 1, 2003)
    %
    % Multiply a bidimensional marginal subtable by a ratio of
    % scalars. This function is used by iterativerescaling.m
    %
    % SYNTAX :
    %
    % [P]=multiplysubtable(P,dim,index,ptheor,pest);
    %
    % INPUT :
    %
    % P         nc by ... by nc   ndim-dimensional table with nc elements along
    %                             each one of the ndim dimensions.
    % dim       1 by 2            vector that refers to the dimensions to be
    %                             considered in the nc by nc subtable.
    % index     1 by 2            vector that refers to the location of the value
    %                             to be considered in the nc by nc subtable
    % ptheor    scalar            theoretical value for the bivariate probability
    %                             in the marginal subtable at the position specified
    %                             by index.
    % pest      scalar            estimated value for the bivariate probability
    %                             in the marginal subtable at the position specified
    %                             by index. The marginal subtable is obtained by
    %                             summation over all dimensions of P except for 
    %                             dimensions dim(1) and dim(2).
    %
    % OUTPUT :
    %
    % P         nc by ... by nc   ndim-dimensional table with nc modified elements
    %                             along each one of the ndim dimensions.
    '''

    # Create a list of slices
    slices = [slice(None)] * P.ndim  # Start with all slices
    slices[dim[0]] = index[0]  # Set the first dimension index
    slices[dim[1]] = index[1]  # Set the second dimension index

    # Convert to a tuple
    slices = tuple(slices)
    Pfit = P.copy()
    if ptheor == 0:
        Pfit[slices] = 0
    else:
        Pfit[slices] *= ptheor / pest

    return Pfit

def iterativerescaling(Pbiv,tol):
    '''
    % iterativerescaling     - iterative rescaling algorithm for maximum entropy fitting
    %                          (December 1, 2003)
    %
    % Iterative rescaling algorithm for the fitting of a multidimensional
    % probability table. This function is used by maxentropytable.m
    %
    % SYNTAX :
    %
    % [Pfit]=iterativerescaling(Pbiv,ndim,nc,tol);
    %
    % INPUT :
    %
    % Pbiv     ndim by ndim     square cell array where each upper diagonal cell (i,j) is
    %                           a symmetric nc by nc matrix of bivariate probabilities
    %                           for categories considered at locations separated by a
    %                           given distance, where ndim is the number of locations.
    % ndim     scalar           dimensionality of Pbiv.
    % nc       scalar           number of elements along each dimension of Pbiv, that also
    %                           corresponds to the number of categories for the variable.
    % tol      scalar           value of the stopping criterion for the iterative scaling,
    %                           that corresponds to the maximum of the absolute differences
    %                           between joint probabilities estimates for two successive
    %                           iterations. Default value is equal to 1e-3.
    %
    % OUTPUT :
    %
    % Pfit    nc by ... by nc   ndim-dimensional table with nc elements along each
    %                           dimension, that contains joint probability estimates
    %                           after iterative rescaling is completed.
    '''
    ndim, nc = len(Pbiv),len(Pbiv[0][1])
    Pfit=np.ones([nc]*ndim)/(nc**ndim)
    Pfitold=Pfit.copy()

    niter=0
    while True:
        niter=niter+1
        for i in range(ndim):
            for j in range(i+1,ndim):
                Pmar = sumoverallexcepttwo(Pfit, i, j)
                Pest = np.squeeze(Pmar) # estimated value for the bivariate probability

                Ptheor = Pbiv[i][j] # theoretical value for the bivariate probability
                
                ## if nan, mutiply ratio equal 1
                rescale_ratio = np.where(~np.isnan(Ptheor), Ptheor / Pest, 1)
                
                # Reshape the 2D array for broadcasting
                expand_shape = [1] * Pfit.ndim
                expand_shape[i] = expand_shape[j] =  nc
                
                # rescale by theoritical value to get maximum entropy
                Pfit*=rescale_ratio.reshape(expand_shape)

        if np.max(abs(Pfit-Pfitold))<tol:
            break
        else:
            Pfitold=Pfit.copy()

    return Pfit,niter

def maxentropytable(c,dmodel,Pmodel,tol = 1e-3):
    '''
    獲得在一組已知點位下，不同已知值組合下其對應未知點的聯合機率分佈
    % maxentropytable        - estimate the maximum entropy multivariate probability table
    %                          (December 1, 2003)
    %
    % Estimate the maximum entropy multivariate probability table from
    % the knowledge of the bivariate probabilities between categories
    % for given distances. The algorithm which is used is the iterative
    % rescaling procedure.
    %
    % SYNTAX :
    %
    % [Pfit,niter]=maxentropytable(c,dmodel,Pmodel,tol);
    %
    % INPUT :
    %
    % c         n  by d    matrix of coordinates for the locations where the
    %                      multivariate probabilities for the categories have
    %                      to be estimated. A line corresponds to the vector
    %                      of coordinates at a location, so the number of columns
    %                      is equal to the dimension of the space. There is no
    %                      restriction on the dimension of the space.
    % dmodel    nd by 1    vector of values for distances for which the bivariate
    %                      probabilities between categories have been modeled. 
    % Pmodel    nc by nc   cell array, where each cell is a nd by 1 vector of
    %                      bivariate probability values between two categories at
    %                      distances specified in dmodel.
    % tol       scalar     value of the stopping criterion for the iterative scaling,
    %                      that corresponds to the maximum of the absolute differences
    %                      between joint probabilities estimates for two successive
    %                      iterations. Default value is equal to 1e-3.
    %
    % OUTPUT :
    %
    % Pfit   n by ... by n   n-dimensional table of estimated joint probability values
    %                        with nc elements along each of the n dimensions.
    % niter  scalar          number of iterations for reaching the stopping criterion.
    '''
    ncat=Pmodel.shape[0]
    ndim=len(c)

    #### Build the bivariate probability tables

    Pbiv = []
    for i in range(ndim):
        Pbiv.append([])
        for j in range(ndim):
            Pbiv[i].append([]) ## corresponding to matlab code Pbiv=cell(ndim,ndim); create ndim,ndim list 
            
    for i in range(ndim):
        for j in range(i+1,ndim):
            [d]=coord2dist(c[[i]],c[[j]])
            Pbiv[i][j] = probamodel2bitable(d[0],dmodel,Pmodel)
    #### Estimate the maximum entropy table

    Pfit,niter=iterativerescaling(Pbiv,tol)

    
    return Pfit,niter

def producttablepdf(P,pdf):
    '''
    % productablepdf         - product between ndim margins and a ndim-dimensional table
    %                         (December 1, 2003)
    %
    % Product between ndim margins and a ndim-dimensional table.
    % This function is used by BMEcategPdf.m for taking into account
    % a probability distribution function at an estimation location
    %
    % SYNTAX : 
    %
    % [Pprod]=producctablepdf(P,pdf);
    %
    % INPUT :
    %
    % P           nc by ... by nc   ndim-dimensional table with nc elements along
    %                               each one of the ndim dimensions.
    % pdf         ndim by nc        matrix of probability values. Each line correspond
    %                               to a specific margin and each column corresponds to
    %                               a category, so that each line must sum up to 1.
    %        
    % OUTPUT :
    %
    % Pprod       nc by ... by nc   ndim-dimensional table obtained by multiplying P by
    %                               each line of pdf along its 1-dimensional margins.

    '''
    ndim = P.ndim
    nc = pdf.shape[-1]
    
    Pprod = P.copy()

    for i in range(ndim):
        slices = [slice(None)] * ndim
        for j in range(nc):
            slices[i] = j
            Pprod[tuple(slices)]*= pdf[i,j]

    return Pprod

def sumvaluesatindex(P,index):
    '''
    % sumvaluesatindex       - sum over several values in a multidimensional table
    %                          (December 1, 2003)
    %
    % Sum the values contained at specified positions in a multidimensional table.
    %
    % SYNTAX : 
    %
    % [S]=sumvaluesatindex(P,index);
    %
    % INPUT :
    %
    % P           nc by ... by nc   ndim-dimensional table with nc elements along
    %                               each one of the ndim dimensions.
    % index       ndim by n         matrix that specify the position of the values
    %                               over which summation should occur. Each column
    %                               corresponds to the position of an element in P.
    %
    % OUTPUT :
    %
    % S           scalar            that corresponds to the sum of the values of P
    %                               located at the n positions specified by index.

    '''
    
    ndim=P.ndim
    stridx=''
    for i in range(ndim):
        stridx_ = [':']*ndim
        stridx_[i] = str(np.where(index[i,:]==1)[0]).replace(' ',',')
        stridx_ = ','.join(stridx_)
        stridx = stridx+ '['+stridx_+']'
    ###
    S = eval(f'P{stridx}.sum()')
    return S

def BMEcatPdf(ck,cs,ps,dmodel,Pmodel,nsmax,dmax,options = [0,1e-3]):
    '''
    BMEcatPdf              - categorical data prediction using BME with pdf data
                              (December 1, 2003)
    
    Prediction of the conditional categorical probability distribution
    function at a set of locations using the knowledge of the probability
    distributions of categories at neighbouring locations. The conditional
    distribution is obtained by conditioning a probability table that has
    been built using a maximum entropy algorithm that incorporates the
    knowledge of the bivariate probabilities between two categories as a
    function of the distance between the corresponding locations.
    
    For prediction using both hard and soft information, see the note at
    the end of this help.
    
    SYNTAX : 
    
    pk = BMEcatPdf(ck,cs,ps,dmodel,Pmodel,nsmax,dmax,options);
    
    Args:
        ck (ndarray): nk by d.    
            matrix of coordinates for the locations where the
            probabilities for each category have to be estimated.
            A line corresponds to the vector of coordinates at a 
            location, so the number of columns is equal to the 
            dimension of the space. There is no restriction on the
            dimension of the space.
        cs (ndarray): ns by d.    
            matrix of coordinates for locations where distributions
            of categories are known, with same convention as for ck.
        ps (ndarray): ns by nc.
            matrix of [0,1] real values for the probabilities of the
            categories at the coordinates specified in cs. Each line
            correspond to a location and each column to a category,
            so that each line must sum up to 1.
        dmodel (ndarray): nd by 1.
            vector of values for distances for which the bivariate
            probabilities between categories have been modeled. 
        Pmodel (ndarray): shape of (nc,nc,nd)   
            3D numpy array, which is the  bivariate probability values 
            between two categories at certain distance which specific in dmodel.
        nsmax (scalar):      
            maximum number of locations in the neighbourhood used
            for the estimation at the locations specified in ck.
        dmax (scalar or ndarray):1 or 1 by 3.    
            maximum distance between an estimation location and
            locations where categories are known. All locations
            separated by a distance smaller than dmax from an
            estimation location will be included in the estimation
            process for that location, whereas other locations are 
            neglected.
        options (list): 1 by 2.
            optional vector of parameters that can be used if the
            default values are not satisfactory (otherwise it can simply
            be omitted from the input list of variables). 
            options[0] is taking the value 1 or 0 if the user wants or
            does not want to display the order number of the location
            which is currently processed, respectively. Default value
            is equal to 0.
            options[1] is the stopping criterion when fitting the
            maximum entropy table. Default value is equal to 1e-3
            (see maxentropytable.m).
    
    Returns:
    
        pk (ndarray): nk by nc.   
            matrix of conditional probability values, where each
            column refers to a category and each line refers to an
            estimation location. Each line sum up to one. Values coded
            as NaN mean that no estimation has been performed at that 
            location due to the lack of available data. 
    
    NOTE: 
        This program can process any kind of mixture of hard and soft information
        using the following conventions :
    
            (1) for a hard data (the jth category is observed at the ith location),
                the corresponding ith line ps(i,:) has its jth probability value
                equal to one and other probability values equal to 0.
            (2) for a subset of n possible categories at the ith location, the
                corresponding ith line ps(i,:) will have probability values
                equal to 1/n for the elements corresponding to these categories
                whereas other elements have a zero probability value.
           
        Programs BMEcatHard.m and BMEcatSubset.m which are both specific cases
        of BMEcatPdf.m have been provided for the user's facility. They will
        provide the same results.
    
    Examples:
        >>> pk = BMEcatPdf(ck,cs,ps,dmodel,Pmodel,nsmax,dmax,options)
    '''
    
    isnotOK=np.sum(abs(np.diff(ps.T,axis = 0)),axis = 0)==0
    cs = cs[~isnotOK,:]
    ps = ps[~isnotOK,:] 
    
    nk = len(ck)
    ncat=Pmodel.shape[0]
        
    pk=np.ones((nk,ncat))*np.nan;## 先創一個都是nan的矩陣再慢慢取代
    for i in range(nk):
        ck0=ck[[i],:]

        ## 找臨近的位置 ds為每個被找的點與ck0之距離 sumnslocal為總共找到的已知點數 ck0為未知點位, cs與ps分別為已知點位與機率
        ## cslocal,pslocal分別為鄰近已知點座標與機率
        cslocal,pslocal,ds,sumnslocal,_=neighbours(ck0,cs,ps,nsmax,dmax)
 
        if sumnslocal>0:
            isdszero=np.where(ds==0)[0]
            if isdszero.size ==0:
                test=0
            else:
                ps0=pslocal[isdszero[0],:]
                pslocal = np.delete(pslocal, isdszero[0], 0)
                cslocal = np.delete(cslocal, isdszero[0], 0)
                sumnslocal=sumnslocal-1
                test=1

            ## 從Pmodel與dmodel中找對應距離之間的岩性table,以及其最大熵table
            Pkh_,niter = maxentropytable(np.vstack((ck0,cslocal)),dmodel,Pmodel,options[1])
            
            ## updatting probability table by given points
            Pkh = producttablepdf(Pkh_,np.vstack((np.ones((1,ncat)),pslocal))) 
            sumaxis = tuple(np.arange(1,sumnslocal+1))
            pk[i,:] = Pkh.sum(axis = sumaxis)/ Pkh.sum()

            if test==1:
                pk[i,:]=np.multiply(ps0,pk[i,:])/sum(np.multiply(ps0,pk[i,:]))
        
        if options[0]==1:
            np.disp(f'{i}/{nk}')
        
    return pk