import numpy as np
from scipy.spatial.distance import cdist

def probatablecalc(coord, value, coord_limit, chunk_size = None, ShowCalDis = False):
    '''Estimate the bivariate probability tables for a set of categories which are known at a set of coordinates.(December 1,2003)

    Args:
        coord (ndarray): 2-D ndarray of floats with shape (n, d). Coordinates for the locations where the categories are known. A row corresponds to the vector of coordinates at a location, so the number of columns is equal to the dimension of the space. There is no restriction on the dimension of the space.
        value (ndarray): 2-D ndarray of floats with shape (n, nc). Probability for the categories at the coordinates specified in c. 
        coord_limit (ndarray): 1-D ndarray of floats with shape (ncl,). Limits of the distance classes that are used for estimating the probability tables. Distance classes are open on the left and closed on the right. The lower limit for the first class is >=0.
        chunk_size (int): Split the dataset into chunks during calculation. The number of points in each chunk will be close to the specified chunk_size. Smaller chunk sizes will use less memory but take more time. The result will be the same no matter what chunk_size is. The default is None, which means the dataset will not be split during calculation.
        ShowCalDis (bool): Print the calculating process.

    Returns:
        D (ndarray): 1-D ndarray of floats with shape (ncl,). Sorted values of the mean distance separating the pairs of points that belong to the same distance class. The first value corresponds to a null distance.
        P (ndarray): 3-D ndarray of floats with shape (nc, nc, ncl). Symmetric array of cells that contains the bivariate probability tables estimates between the nc categories for the distance classes specified in D. Diagonal cells contain the ncl by 1 array of probability estimates for the same category, whereas off-diagonal cells contain the ncl by 1 array of cross-category probability estimates. For diagonal cells, the first value of each array is the estimated proportion of the corresponding category, whereas this first value is always equal to 0 for off-diagonal cells.
        O (ndarray): 1-D ndarray of integers with shape (ncl,). Number of pairs of points that belong to the corresponding distance classes. The first value corresponds to the number of locations specified in c.

    Note:
        1. The D, P, and O output variables can be used without modification as input for the probatableplot.m and probatablefit.m functions.
        2. When a distance class does not contain any pairs of points, the function outputs a warning message. The D and P elements for the corresponding distance class are thus coded as NaN's, whereas the corresponding O element is equal to 0.

    '''
    if chunk_size is None:
        chunk_size = len(coord)

    #compute the distances
    coord = np.array(coord)
    n_data = coord.shape[0]
    
    value = np.array(value)
    n_cata = value.shape[1]
    
    coord_limit = np.array(coord_limit).flatten()
    n_limit = coord_limit.shape[0]
    
    #init
    P_sum = np.empty((n_cata, n_cata, n_limit))
    P_sum[:] = 0
    P_count = np.empty((n_cata, n_cata, n_limit))
    P_count[:] = 0
    D_sum = np.empty((n_limit,))
    D_sum[:] = 0
    O = np.empty((n_limit,))
    O[:] = 0
    
    coord_split = np.array_split(coord, np.ceil(coord.shape[0]/chunk_size))
    value_split = np.array_split(value, np.ceil(value.shape[0]/chunk_size))
    if ShowCalDis:
        chnum = len(coord_split)
        print(f'Total chunks: {chnum}')
        print('Processing...')
    for n,[coord_i, value_i] in enumerate(zip(coord_split, value_split)):
        for coord_j, value_j in zip(coord_split, value_split):
            n_data_i = coord_i.shape[0]
            n_data_j = coord_j.shape[0]
            dis = cdist(coord_i, coord_j)
            prob = value_i.reshape((n_data_i, 1, n_cata, 1)) * value_j.reshape((1, n_data_j, 1, n_cata))
            prob /= prob.sum(axis=(-1,-2), keepdims=True)
    
            #process 0 distance
            idx_row, idx_col = np.where(dis == 0)
            select_dis = dis[idx_row, idx_col]
            D_sum[0] += select_dis.sum()
            O[0] += select_dis.shape[0]
            select_prob = prob[idx_row, idx_col]
            P_sum[:,:,0] += select_prob.sum(axis=0)
            P_count[:,:,0] += select_prob.shape[0]

            #process other
            for idx, (left, right) in enumerate(zip(coord_limit[:-1], coord_limit[1:])):
                idx_row, idx_col = np.where((dis > left) & (dis <= right))
                select_dis = dis[idx_row, idx_col]
                
                D_sum[idx+1] += select_dis.sum() / 2
                O[idx+1] += select_dis.shape[0] / 2
                select_prob = prob[idx_row, idx_col]
                P_sum[:,:,idx+1] += select_prob.sum(axis=0)
                P_count[:,:,idx+1] += select_prob.shape[0]
                
        if ShowCalDis:
            print(f'{n+1}/{chnum}')

    D = D_sum / O
    P = P_sum / P_count
    return D, P, O