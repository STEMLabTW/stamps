# -*- coding: utf-8 -*-
"""
Created on Tue Nov 17 10:37:12 2015

@author: hdragon689
"""

import numpy as np

from ..general.coord2K import coord2K
from ..stats.anisotropy import aniso2iso

def simuchol(ch,covmodel,covparam,ns=1):
    '''
    <p style='text-align: justify;'> 
    Simulation by the Cholesky method.
    Implementation of the traditional non conditional
    simulation method based on a Choleski decomposition
    of the covariance matrix. This simulation method is
    especially recommended for simulating independantly
    several sets of a limited number of hard values
    (less than few hundreds). Simulated values are zero
    mean Gaussian distributed. 
    </p>
    Args:
        ch (ndarray): shape of (nh, d). matrix of coordinates for the nh locations with d dimension
            where hard data have to be simulated. There is no restriction on the dimension of the space.
        covmodel (list): List of string that contains a sequence of covariance models string.
        covparam (list): List of float that contains a sequence of covariance parameters values.
        ns (int): number of sets of simulated values.

    Returns:
        Zh (ndarray):shape of (nh, ns). ns set of simulation results of given coordinates.

    Examples:
        >>> from stamps.stats.simuchol import simuchol

        Create a coordinates which wanted to simulate value        
        >>> xi,yi = np.meshgrid(range(1,200,5),range(1,200,5))
        >>> ch = np.vstack((xi.ravel(),yi.ravel())).T

        Specify the covariance model and parameter.
        >>> covmodel = ['exponentialC']
        >>> covparam = [(7,100)]

        Simulate values on given coordinate by the covariance model
        >>> Zh=simuchol(ch,covmodel,covparam,ns = 1)
    '''

#rand('state',sum(100*clock));
    K,_ = coord2K(ch,ch,covmodel,covparam)
    L = np.linalg.cholesky(K)
#  Lt=L.T
#  n=Lt.shape[0]
    n = K.shape[0]
    Zh = np.zeros([n,ns])
    for i in np.arange(ns): 
        Zh[:,i:i+1] = L.dot(np.random.randn(n,1))
  
    return Zh  
    
def anisosimuchol(ch,covmodel,covparam,theta,ratio,ns=1):
    '''
    <p style='text-align: justify;'> 
    Simulation by the Cholesky method in given anisotropic axis.
    Implementation of the traditional non conditional
    simulation method based on a Choleski decomposition
    of the covariance matrix. This simulation method is
    especially recommended for simulating independantly
    several sets of a limited number of hard values
    (less than few hundreds). Simulated values are zero
    mean Gaussian distributed. 
    </p>
    Args:
        ch (ndarray): shape of (nh, d). matrix of coordinates for the nh locations with d dimension
            where hard data have to be simulated. There is no restriction on the dimension of the space.
        covmodel (list): List of string that contains a sequence of covariance models string.
        covparam (list): List of float that contains a sequence of covariance parameters values.
        ns (int): number of sets of simulated values.
        theta (float or list): scalar or vector of angle values that characterize the anisotropy. 
                In a two dimensional space, theta is the trigonometric angle
                between the horizontal axis and the principal axis of the
                ellipse. In a three dimensional space, spherical coordinates
                are used, such that theta(1) is the horizontal trigonometric
                angle and theta(2) is the vertical trigonometric angle for the
                principal axis of the ellipsoid. All the angles are measured
                counterclockwise in degrees and are between -90 and 90.
        ratio (float or list): scalar or vector that characterize the ratio for the length of the axes
                for the ellipse (in 2D) or ellipsoid (in 3D). In a two dimensional
                space, ratio is the length of the secondary axis of the ellipse
                divided by the length of the principal axis, so that ratio in [0,1]. In a
                three dimensional space, ratio(1) is the length of the second
                axis of the ellipsoid divided by the length of the principal axis, 
                whereas ratio(2) is length of the third axis of the ellipsoid
                divided by the length of the principal axis, so that ratio(1) and
                ratio(2) are both in the range of [0,1].

    Returns:
        Zh (ndarray):shape of (nh, ns). ns set of simulation results of given coordinates.

    Examples:
        >>> from stamps.stats.simuchol import simuchol

        Create a coordinates which wanted to simulate value        
        >>> xi,yi = np.meshgrid(range(1,200,5),range(1,200,5))
        >>> ch = np.vstack((xi.ravel(),yi.ravel())).T

        Specify the covariance model and parameter.
        >>> covmodel = ['gaussianC']
        >>> covparam = [(3,30)]

        Specify the ratio and theta of anisotropic condition.
        >>> theta = 60
        >>> ratio = 0.8

        Simulate values on given coordinate by the covariance model
        >>> Zh=anisosimuchol(ch,covmodel,covparam,theta = theta,ratio = ratio,ns = 1)
    '''

    newch = aniso2iso(ch,theta,ratio)
    K,_ = coord2K(newch,newch,covmodel,covparam)
    L = np.linalg.cholesky(K)
#  Lt=L.T
#  n=Lt.shape[0]
    n = K.shape[0]
    Zh = np.zeros([n,ns])
    for i in np.arange(ns): 
        Zh[:,i:i+1]=L.dot(np.random.randn(n,1))
    
    return Zh  

if __name__ == "__main__":  
    pass