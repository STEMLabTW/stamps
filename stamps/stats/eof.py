# -*- coding: utf-8 -*-
"""
Created on Tue May 26 18:57:18 2015

@author: hdragon689
"""
from six.moves import range, xrange
from six import print_

import numpy as np
import scipy.sparse.linalg as ssl
import warnings

from scipy.linalg import svd, diagsvd

try:
    from sklearn import mixture
except Exception as e:
    print_('warning: some functions cannot be used because:', e)


def eof( X, m='all', centered=True, normalized=True, method='svd' ):

    ''' Empirical Orthogonal Function - computes EOF of a matrix.
    
    Args:
        X (ndarray): shape of (k, n).The matrix with n monitoring sites and k measuring events. 
            on which to perform the EOF.  
        m (int): the number of EOFs to return.  If num='all'(default),then all EOFs are returned.  
        centered (bool): center flag. If it is True, then all time series 
            are centered by minus their mean before EOFs are computed.
        normalized (bool): normlaization flag. If it is true, then all time series 
            are normalized by their standard deviation before EOFs 
            are computed.  In this case,the 7-th 
            output argument will be the standard deviations of each column.
        method (string): method to be used for EOF calculation. svd or eig.
            svd refers to singular value decomposition (default) and eig refers to eigenvalue decomposition. In the
            case of eig method, M should be a square matrix of the covariance functions to be assessed

    Returns:
        L (ndarray): shape of (m,). 1D array for the eigenvalues of the covariance matrix 
            ( i.e. they are normalized by 1/(m-1), where m is the number of rows ).  
        s (ndarray): shape of (m,). 1D array for singular values (n length for full matrix)
        PCs (ndarray): shape of (k, m). unitary matrix having left singular vectors as columns
        EOFs (ndarray): shape of (n, m). principal components. unitary matrix haveing right 
            sigular vectors as rows
        ECs (ndarray): shape of (k, m). expansion coefficients.
        error (ndarray): shape of (n,). 1D array of the reconstruction error (L2-norm) for n monitoring sites.
        norms (ndarray): shape of (n,). 1D array for standard deviation of each item or one Principle                   
            coefficients (PCs in other terminology) and error is the reconstruction error (L2-norm).
        explained_variance (ndarray): shape of (m,). Explained_variance of each EOF components.
    
    ### Remark:

        - m is eqaul n when full matrix is used.
        - Data is not detrended before handling.  If needed, perform the detrending before using this function to fix that.
        - This code is modified from the Matlab code by David M. Kaplan in 2003 by Hwa-Lung Yu 2015/5/31
    
    Examples:
        >>> from stamps.stats.eof import eof
        >>> L, s, PCs, EOFs, ECs, error, norms, explained_variance = eof(X)
    '''  

    def rSVD(A, r, t=0, direction='default', Omega=None, n_rand=1, multi=False, queue=None):
        '''
        %-----------------------------------------------
        % Randomized truncated SVD
        % Input:
        %   A: m*n matrix to decompose (Need to be a numpy array)
        %   r: the number of singular values to keep (rank of S)
        %   t (optional): the number used in power (default = 0)
        %   direction (optional): to sample columns (=0) or sample rows (=1)
        %                         (default = 0, if m <= n
        %                                  = 1, if m > n )
        %   Omega (optional): the projection matrix to use in sampling
        %                     (program will compute A * Omega for direction = 0
        %                                           Omega * A for direction = 1)
        %
        % Output: classical output as the builtin svd matlab function
        %-----------------------------------------------
        '''

        m, n = A.shape
        U = np.zeros((m, r, n_rand))
        V = np.zeros((n, r, n_rand))
        S = np.zeros((r, r, n_rand))
        
        for k in range(n_rand):

            if direction == 'default':
                if m <= n:
                    direction = 0
                else:
                    direction = 1

            if direction == 0:
                Omega = np.random.randn(n, r)/np.sqrt(n)
            else:
                Omega = np.random.randn(m, r)/np.sqrt(m)

            if direction == 1:
                A = A.T
                Omega = Omega.T

            # Compute SVD
            if t == 0:
                Y = np.dot(A, Omega)
            else:
                Y = np.dot(np.dot(A, A.T)**t, np.dot(A, Omega))
            Q, _ = linalg.qr(Y, mode='economic')
            B = np.dot(Q.T, A)
            U_tild, S_temp, Vh_tild = linalg.svd(B, full_matrices=0)

            if direction == 0:
                U_temp = np.dot(Q, U_tild)
                Vh_temp = Vh_tild
            else:
                Vh_temp = np.dot(Q, U_tild)
                U_temp = Vh_tild

            V_temp = Vh_temp.T
            S_temp = linalg.diagsvd(S_temp, r, r)
            U[:, :, k] = U_temp[:, :r]
            S[:, :, k] = S_temp[:r, :r]
            V[:, :, k] = V_temp[:, :r]

        if multi:
            queue.put([U, S, V])
        else:
            return U, S, V

    def combine_orth(W_0, U_tild, tol_t=1e-2, tol_F=1e-4, iter_max=100):
        '''
        %-----------------------------------------------
        % Combine U_i to W
        % Input:
        %   W_0: the initial matrix used in updating. (Maybe U_0 is good.)
        %   U_tild: U_tild(:, :, i) = U_i
        %   tol_t(optional): stepsize < tol_t will stop. (default = 1e-2)
        %   tol_F(optional): 0 < (F_new -F_old)/F_old < tol_F will stop (default = 1e-3)
        %   iter_max(optional): max number of iteration.(default = 100)
        %
        % Output:
        %   W: the combined result
        %   n_iter: number of iterations
        %   W_record: W_record(:, :, i) = i_iter-th combined result
        %-----------------------------------------------
        '''
        
        m, r, n = U_tild.shape
        U_tild = np.reshape(U_tild, (m, n*r))
        W_record = np.zeros((m, r, iter_max))

        W = W_0
        GF = np.dot(U_tild, np.dot(U_tild.T, W))/n
        n_iter = 0
        F_old = np.trace(np.dot(GF.T, W))
        t = 1
        
        while n_iter < iter_max:
            # Calculate the gradient
            U = np.hstack([GF, W])
            V = np.hstack([W, -GF])
            Y = W + t*(GF - np.dot(W, np.dot(W.T, GF)))/2
            X = np.dot(np.linalg.inv(-np.eye(2*r) + t*np.dot(V.T, U))/2, np.dot(V.T, Y))
            #  Updating
            W_new = Y - t*np.dot(U, X)/2
            
            F_new = np.trace(np.dot(np.dot(W_new.T, U_tild), np.dot(U_tild.T, W_new))/n)

            while F_new < F_old:
                t = t/2
                if t < tol_t:
                    print_('The step size is too small.')
                    W_record[:, :, n_iter+1:-1] = []
                    break
                Y = W + t/2*(GF - np.dot(W, np.dot(W.T, GF)))
                X = np.dot(np.linalg.inv(np.eye(2*r) + t/2*np.dot(V.T, U)), np.dot(V.T, Y))
                W_new = Y - t/2*np.dot(U, X)

                F_new = np.trace(np.dot(np.dot(W_new.T, U_tild), np.dot(U_tild.T, W_new))/n)

            if (F_new-F_old)/F_old < tol_F:
                print_("The change rate(%f) is too small." % ((F_new-F_old)/F_old))
                break
            else:
                W = W_new
                GF = np.dot(U_tild, np.dot(U_tild.T, W))/n
                F_old = F_new
                W_record[:, : , n_iter] = W
                n_iter += 1
                
        W_record = np.delete(W_record, np.s_[n_iter:iter_max], 2)

        return W, n_iter, W_record

    # EOF start
    # Centered by 
    if centered:
        mean = X.mean(axis=0)
        X = X - mean
    # Normalized by standard deviation if desired
    if normalized:
        norms = np.std(X, axis=0)
        X = np.dot(X, np.diag(1/norms))
    else:
        norms = []

    # Using SVD to solve the eigen problem of covariance matrix
    n, p = X.shape
    rk = np.linalg.matrix_rank(X)

    if method=='svd':
        # To solve with full matrices
        if m=='all':
            U, s, Vh = svd(X, full_matrices=False)
            PCs = U
            EOFs = Vh.T
        else:
            if m > rk:
                m = rk
        # To solve with the first m singular vectors/values
        # sparse的svd需要先sort才能用!!!
            U, s, Vh = ssl.svds(X, m)
            sort_idx = np.argsort(-s)
            s = s[sort_idx]
            PCs = U[:,sort_idx]
            EOFs = Vh[sort_idx,:].T
        
        # To compute eigenvalues
        L = s**2/(n-1)

        # Check computation errors
        ECs = np.dot(PCs, np.diag(s))
        diff = X - np.dot(ECs,EOFs.T)
        error = np.sqrt(np.sum(diff*np.conj(diff),axis = 0))

    # To Solve the eigen problem of covariance matrix directly
    else:
        CU = 1./(n-1)*X.T.dot(X)
        L, EOFs = np.linalg.eig(CU)
        if np.any(np.iscomplex(EOFs)):
            EOFs = EOFs.real
            L = L.real
        ECs = (EOFs.T.dot(X.T)).T
        s = np.sqrt(L)    
        s[np.where(np.isnan(s))] = 0.
        PC = ECs*1/s # the dimension of L should be checked
        diff = X - np.dot(ECs,EOFs.T)
        error = np.sqrt(np.sum(diff*np.conj(diff),axis = 0))

    explained_variance = np.zeros(len(L))
    for n_f in range(len(L)):
        explained_variance[n_f] = np.linalg.norm(np.dot(ECs[:,n_f:n_f+1], EOFs[:,n_f:n_f+1].T))**2/np.linalg.norm(X)**2

    return L, s, PCs, EOFs, ECs, error, norms, explained_variance

''' Extended EOF '''

def eeof( X, M, m='all', centered=True ):
    '''
    Extended Empirical Orthogonal Function - computes EEOF of a matrix.
    
    Usage: [L, lambda, PC, EOFs, EC, error, norms] = EEOF( M, tl, n, ... )
    
    Input: 
    X        n by p         the matrix with n obs and p items on which to perform 
                            EEOF.  
    M        integer        Window size to assess the time lags. In this case, the 
                            space-time dynamic pattern of size of M by p is assessed                 
    m        scalar/string  the number of EOFs to return.  If num='all'(default), 
                            then all EOFs are returned.
    centered bool

    Output:
    L
    s
    EPCs    list          the n-tlag principal extended EOFs. Each has size of 
                                                tlag by p. For the space-time case, p is the number of 
                                                stations
    EEOFs     n-tl by n-tl  Each column contains the extended EC for the corresponding
                                                extended EOF
 
    '''

    # EEOF start

    n,p = X.shape

    if centered:
        mean = X.mean(axis=0)
        X = X - mean

    # To construct the time lagged data matrix (the grand matrix)
    Chi_X = np.zeros( (n-M+1, p*M) )
    X_T = X.T
    for lag in range(M+1):
        x_lag = np.roll(X_T, -lag, axis=1)
        x_lag = x_lag[:,:M].ravel()
        Chi_X[lag,:] = x_lag

    # Computing the eigenvalues/eigenvectors of the grand covariance matrix 
    if m=='all':
        U, s, Vh = svd(Chi_X)
        EPCs = U
        EEOFs = Vh.T
    else:
        U, s, Vh = ssl.svds(Chi_X, m)
        EPCs = U[:,::-1]
        EEOFs = Vh[::-1,:].T

    # To compute eigenvalues
    L = s**2/(n-1)

    return L, s, EPCs, EEOFs


def meof( X, m='all', centered=True, stack_direc=0 ):

    '''
    Multivariate Empirical Orthogonal Function - Computing multivariate EOF
    '''

    n_v = len(X)
    mvX = np.concatenate(X, axis=stack_direc)

    L, s, PCs, mvEOFs, mvECs, error, norms = eof(mvX, m, centered=centered, normalized=False)


    return L, s, mvEOFs, mvECs, n_v


''' varimax function'''   
def varimax_( Phi, gamma=1.0, q=50, tol=1e-7 ):
    '''
    This code is obtained from
    https://en.wikipedia.org/wiki/Talk%3aVarimax_rotation
    '''  

    p, k = Phi.shape
    R = np.eye(k)
    d=0
    for i in xrange(q):
        d_old = d
        Lambda = np.dot(Phi, R)
        u, s, vh = svd(np.dot(Phi.T, Lambda**3 - (gamma/p) * Lambda.dot(np.diag(np.diag(Lambda.T.dot(Lambda))))))
        R = np.dot(u,vh)
        d = np.sum(s)
        if d_old!=0 and d/d_old < 1 + tol: break
    
    return np.dot(Phi, R), R


def sreof( X, m='all', centered=True, normalized=True, *args ):
    '''Scaled and rotaed EOF - computes scaled and rotated EOF of a matrix. 
    
    Args:
        X (ndarray): shape of (k, n).The matrix with n monitoring sites and k measuring events. 
            on which to perform the EOF.  
        m (int): the number of EOFs to return.  If num='all'(default),then all EOFs are returned.  
        centered (bool): center flag. If it is True, then all time series 
            are centered by minus their mean before EOFs are computed.
        normalized (bool): normlaization flag. If it is true, then all time series 
            are normalized by their standard deviation before EOFs 
            are computed.  In this case,the 7-th 
            output argument will be the standard deviations of each column.
        *args (properties): args are used to give the parameter also in eof function.

    Returns:
        L (ndarray): shape of (m,). 1D array for the eigenvalues of the covariance matrix 
        s (ndarray): shape of (m,). 1D array of the eigenvalues of the EOFs
        NewEOFs (ndarray): shape of (n, m). The EOF results are listed in the columns.
        NewECs (ndarray): shape of (k, m). The correponding EC results are listed in the columns
        explained_variance (ndarray): shape of (m,). Explained_variance of each EOF components.
       
    ### Remark:
        The result is re-scaled from the original EOF results by considering the 
        eigenvalues of each EOFs. The rotation is performed on the scaled EOFs.
        By doing this, the EOF rotation can have relatively low impacts from the number
        of EOFs to be rotated and therefore the results can be stabilized. 

    Examples:
        >>> from stamps.stats.eof import sreof
        >>> L, s, NewEOFs, NewECs, explained_variance = sreof(X)
    '''  

    n,p = X.shape
    L, s, PCs, EOFs, ECs, error, norms, explained_variance = eof(X, m, centered, normalized,*args)

    if m == 'all':
        SEOF = EOFs.dot(np.diag(s))
    else:
        SEOF = EOFs.dot(np.diag(s)) # scaled EOFs by singular values
    
    # rotate the EOFs by using varimax method  
    # rotm,REOF,__= varimax(SEOF) # this is a slower varimax
    # REOF=np.dot(EOFs,rotm)
    REOF, rotm = varimax_(SEOF)
    REOF = np.dot(EOFs, rotm)
    
    # rotate the corresponding ECs
    # NewEC=np.dot(EC,np.linalg.inv(rotm.T))
    NewEOFs = REOF
    NewECs = np.dot(ECs[:,:rotm.shape[0]],np.linalg.inv(rotm.T))

    m = rotm.shape[0]

    for i in xrange(m):
        maxi = np.where((np.abs(NewEOFs[:,i]).max()==np.abs(NewEOFs[:,i])))
        signi = float(NewEOFs[:,i][maxi]/np.abs(NewEOFs[:,i][maxi]))
        NewEOFs[:,i] = signi*NewEOFs[:,i]
        NewECs[:,i] = signi*NewECs[:,i]
        
    return L, s, NewEOFs, NewECs, explained_variance

def srmeof( X, m='all', centered=True, stack_direc=0 ):

    n_v = len(X)
    mvX = np.concatenate(X, axis=stack_direc)

    L, s, mvEOFs, mvECs = sreof(mvX, m, centered=centered, normalized=False)

    return L, s, mvEOFs, mvECs, n_v

def varimax(amat,target_basis=None):
    '''
    [ROTM,OPT_AMAT] = varimax(AMAT,TARGET_BASIS)
    
    Gives a (unnormalized) VARIMAX-optimal rotation of matrix AMAT:
    The matrix  AMAT*ROTM  is optimal according to the VARIMAX
    criterion, among all matricies of the form  AMAT*R  for  
    R  an orthogonal matrix. Also (optionally) returns the rotated AMAT matrix
    OPT_AMAT = AMAT*ROTM.
    
    Uses the standard algorithm of Kaiser(1958), Psychometrika.
    
    Inputs:
    
    AMAT          N by K     matrix of "K component loadings"
    TARGET_BASIS  N by N     (optional) an N by N matrix whose columns 
                                                     represent a basis toward which the rotation will 
                                                     be oriented; the default is the identity matrix 
                                                     (the natural coordinate system); this basis need 
                                                     not be orthonormal, but if it isn't, it should be
                                                     used with great care!
    Outputs: 
    
    ROTM         K by K      Optimizing rotation matrix
    OPT_AMAT     N by K      Optimally rotated matrix  (AMAT*ROTM)
    
    Modified by Trevor Park in April 2002 from an original file by J.O. Ramsay  
    Modified by H-L Yu into python code in June 2015 
    '''
    MAX_ITER=50
    EPSILON=1e-7
    
    amatd=amat.shape
    
    if np.size(amatd) != 2:
        raise RuntimeError('AMAT must be two-dimensional')
        
    n=amatd[0]
    k=amatd[1]
    rotm=np.eye(k)
    
    if k==1:
        return
        
    if target_basis==None:
        target_basis_flag=0
        target_basis=np.eye(n)    
    else:
        target_basis_flag=1
        if np.size(target_basis.shape) !=2:
            raise RuntimeError('TARGET_BASIS must be two-dimensional')
        if target_basis.shape==(n,n):
            amat=np.dot(np.linalg.inv(target_basis),amat)
        else:
            raise RuntimeError('TARGET_BASIS must be a basis for the column space')
    
    varnow=np.sum(np.var(amat**2,0))
    not_converged=1
    iterx=0
    while not_converged and iterx < MAX_ITER:
        for j in xrange(k-1):
            for l in xrange(j+1,k):
                # Calculate optimal 2-D planar rotation angle for columns j,l
                phi_max=np.angle(n*np.sum(np.vectorize(complex)(amat[:,j],amat[:,l])**4) \
                                 - np.sum(np.vectorize(complex)(amat[:,j],amat[:,l])**2)**2)/4
                sub_rot = np.array([[np.cos(phi_max),-np.sin(phi_max)],\
                                            [np.sin(phi_max),np.cos(phi_max)]])
                amat[:,[j,l]]=np.dot(amat[:,[j,l]],sub_rot)
                rotm[:,[j,l]]=np.dot(rotm[:,[j,l]],sub_rot)   
                
        varold = varnow
        varnow = np.sum(np.var(amat**2,0))      
    
        if varnow==0:
            return 
        
        not_converged = ((varnow-varold)/varnow > EPSILON)
        iterx= iterx +1
        
    if iterx >= MAX_ITER:
        warnings.warn('Maximum number of iterations reached in function')
    
    if target_basis_flag:  
        opt_amat=target_basis*amat
    else:
        opt_amat=np.dot(amat,rotm)
        
    eps=(varnow-varold)/varnow  

    return rotm, opt_amat, eps  


def eofclass(EOFs,num='all'):
    '''
    To classify the identified EOF locations.  
    
    EOFclass,means,covs,weights,logprob,prob_comps,clf=eofclass(EOFs,num='all')   
    
    Input:
    EOFs      m by n        the EOF (spatial function) results are listed in the 
                                                    columns
    num       scalar/string number of EOFs functions to be classified. Default is 
                                                    'all' that all  functions will be used                           
    
    Output:
    EOFclass  m by num      indicators for the identified regions. 1 denotes the 
                                                    identified locations; otherwise, 0 is shown 
    means     num by 2      the mean of EOF values of the two classified regions.
    covs      num by 2      the variances of EOF values of the two classified 
                                                    regions.
    bound     num by 1      a 1-D array denotes the boundary values to classify 
                                                    EOF values in the num of EOF functions.
    
    Note: The classification is performed by using the two-component Gaussian 
    mixture model to reveal the mean and variance of the close-to-zero EOF region.
    t-test is used to identify the locations which are significantly deviate from 
    zeros.
    '''  
    
    m,n=EOFs.shape
    
    if num is 'all':
        num=n
    
    EOFclass=np.zeros([m,num])
    means=np.zeros([num,2])
    covs=np.zeros([num,2])
    weights=np.zeros([num,2])
#  logprob=[None]*num
#  prob_comps=[None]*num
    clf=[None]*num
    bound=np.zeros(num)
    for i in xrange(num):
        clf[i]=mixture.GMM(n_components=2)
        clf[i].fit(EOFs[:,i:i+1])
        weights[i,0:2]=clf[i].weights_
        means[i,0:2]=clf[i].means_.reshape(2,)
        covs[i,0:2]=clf[i].covars_.reshape(2,)
        signs=means[i,0:2]/np.abs(means[i,0:2])
        popmean=np.min(np.abs(means[i,0:2]))
        idx=np.where(np.abs(means[i,0:2])==popmean)
        popmean=popmean*signs[idx]
        bound[i]=np.sqrt(covs[i,idx])*1.95996
        EOFclass[np.where(EOFs[:,i]>=bound[i]),i:i+1]=1
                
        
#    EOFclass[:,i:i+1]=clf[i].predict(EOFs[:,i:i+1]).reshape(EOFs[:,i].size,1)
#    logprob[i],prob_comps[i]=clf[i].score_samples(EOFs[:,i:i+1])
#    idxmax=np.where(EOFs[:,i]==EOFs[:,i].max())
#    if EOFclass[idxmax,i]==0:
#      EOFclass[np.where(EOFclass[:,i]==0),i]=2
#      EOFclass[np.where(EOFclass[:,i]==1),i]=0
#      EOFclass[np.where(EOFclass[:,i]==2),i]=1
#      weights[i,0],weights[i,1]=weights[i,1],weights[i,0]
#      means[i,0],means[i,1]=means[i,1],means[i,0]
#      covs[i,0],covs[i,1]=covs[i,1],covs[i,0]
#      prob_comps[i][:,[0,1]]=prob_comps[i][:,[1,0]]      
                
#  return EOFclass,means,covs,weights,logprob,prob_comps,clf  
    return EOFclass,means,covs,bound
    
## EOF classification
#EOF1=EOFdf.values[:,0:1]
#clf = mixture.GMM(n_components=2)
#clf.fit(EOF1)
#wei=clf.weights_
#mea=clf.means_
#cov=clf.covars_
#EOF1_classlabel=clf.predict(EOF1).reshape(EOF1.size,1) # preform classification on EOF1
#xxx=np.linspace(EOF1.min(),EOF1.max())
#xxx=xxx.reshape(xxx.size,1)
#plt.figure()
#plt.hist(EOF1,bins=20,normed=True)
#plt.plot(xxx,np.exp(clf.score_samples(xxx)[0]))
#plt.show()



    
if __name__ == "__main__":

    pass
