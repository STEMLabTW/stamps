# -*- coding: utf-8 -*-
"""
Data conversion between space/time vector and space/time grid formats

Created on Sat Jun 27 11:40:16 2015

@author: hdragon689
"""

from six.moves import range
import numpy as np
import pandas as pd

def valstv2stg(ch, zh, cMS=None, tME=None):
    '''
    Converts the values of a space/time variable from a s/t vector 
    format (i.e. the variable z is listed as a vector of n values)
    to a grid format (i.e. the variable Z is given as a nMS by nME matrix 
    corresponding to nMS Monitoring Sites and nME Measuring Events).
    Use help stgridsyntax for information on the s/t grid format.

    Args:
        ch (ndarray): shape of (n, d+1). 
            matrix of space/time coordinates for spatial domain of dimension d
        z (ndarray): shape of (n, 1). 
            vector of field value at coordinate ch
        cMS (ndarray): shape of (nMS, 2). 
            optional. Specify the output Measuring Sites. 
            cMS is equal to the set of all measuring sites appear in the ch if cMS is None.
        tME (ndarray): shape of (nME,). 
            optional. Specify the output Measuring Events.
            tME is equal to the set of all measuring time appear in the ch if nME is None. 
 
    Returns:
        Z (ndarray): shape of (nMS, nME).
            matrix of values for the variable Z corresponding to 
            nMS Monitoring Sites and nME Measuring Event
        cMS (ndarray): shape of (nMS, d).
            matrix of spatial coordinates for the nMS Measuring Sites
        tME (ndarray): shape of (nME,).
            vector of times of the tME Measuring Events
        nanloc (list): The index of missing data in Z.
 
    NOTE : 
        cMS and tME can be provided as input if they are both known.  In that case ch 
        must be a nMS*nME by 3 matrix of the points corresponding to nMS Monitoring Sites
        and nME Measuring Events, listed with space cycling quicker then time.

    Examples:
        >>> from stamps.general.valstvgx import valstv2stg
        
        Assuming there are two observed staions which has randomly assigned observed data in time [1,2,3].
        >>> ch = np.array([[200,300,1], [200,300,2], [200,300,3],
                        [300,300,1], [300,300,2], [300,300,3]])
        >>> zh = np.array([[10.2, 1.02, 25.4, -12.2,0.2, 34.9]]) 

        Coverting input data into s/t grid format.
        >>> Z,cMS,tME,nanloc = valstv2stg(ch,zh)
    '''

    if 'pandas' in str(type(ch)):
        ch = ch.values
    if 'pandas' in str(type(zh)):
        zh = zh.values
    
    cols = ['x','y','t','z']
    data = np.hstack((ch, zh.reshape(zh.size, 1)))
    datadf = pd.DataFrame(data, columns=cols)
    datadf['x'] = datadf['x'].astype(float)
    datadf['y'] = datadf['y'].astype(float)
    datadf['z'] = datadf['z'].astype(np.double)
    dtable = pd.pivot_table(
        datadf, values=datadf.columns[3], index=['y', 'x'], columns=['t'],
        dropna=True
    )
    
    if cMS is not None:
        c_index = pd.MultiIndex.from_arrays(cMS.T, names=('x', 'y'))
        dtable = dtable.swaplevel().reindex(c_index)
    else:
        dtable = dtable.swaplevel()
    cMS = np.array(dtable.index.to_list())
    if tME is not None:
        dtable = dtable.reindex(columns = tME)
    tME = dtable.columns.to_numpy()
    
    Z = dtable.values
   
    # nonlocations
    nanloc=list(zip(np.where(np.isnan(Z))[0],np.where(np.isnan(Z))[1]))

    
    return Z, cMS, tME, nanloc

def valstv2stg2(ch, zh, cMS=None, tME=None):
    '''
    same as valstv2stg but with another method.
    '''

    if 'pandas' in str(type(ch)):
        ch = ch.values
    if 'pandas' in str(type(zh)):
        zh = zh.values
    
    cols = ['x','y','t','z']
    data = np.hstack((ch, zh.reshape(zh.size, 1)))
    datadf = pd.DataFrame(data, columns=cols)
    datadf['x'] = datadf['x'].astype(float)
    datadf['y'] = datadf['y'].astype(float)
    datadf['z'] = datadf['z'].astype(np.double)
    dtable = pd.pivot_table(
        datadf, values=datadf.columns[3], index=['y', 'x'], columns=['t'],
        dropna=True
    )
    
    if cMS is not None:
        c_index = pd.MultiIndex.from_arrays(cMS.T, names=('x', 'y'))
        dtable = dtable.swaplevel().reindex(c_index)
    else:
        dtable = dtable.swaplevel()
    cMS = np.array(dtable.index.to_list())
    if tME is not None:
        dtable = dtable.reindex(columns = tME)
    tME = dtable.columns.to_numpy()
    
    Z = dtable.values
   
    # nonlocations
    nanloc=list(
      zip(
        np.where(np.isnan(Z))[0],
        np.where(np.isnan(Z))[1]
        )
      )
    
    return Z, cMS, tME, nanloc

def valstg2stv(Z, cMS, tME):
    '''
    Converts the coordinates and values of a space/time variable 
    from a grid format (i.e. the variable Z is given as a nMS by nME matrix  
    corresponding to nMS Measuring Sites and nME Measuring Events),
    to a s/t vector format (i.e. the variable z is listed as a vector of nMS*nME values,
    corresponding to points with space/time coordinates, where the spatial coordinate
    cycle quicker than the time coordinates).

    Args:
        Z (ndarray): shape of (nMS, nME).
            matrix of values for the variable Z corresponding to 
            nMS Monitoring Sites and nME Measuring Event
        cMS (ndarray): shape of (nMS, 2).
            matrix of 2D spatial coordinates for the nMS Measuring Sites
        tME (ndarray): shape of (1, nME).
            vector of times of the tME Measuring Events

    Returns:
        ch (ndarray): shape of (nMS x nME,3).
            matrix of space time coordinates, listing the space/time
            locations of the points corresponding to nMS Monitoring Sites
            and nME Measuring Event (space cycles quicker then time) 
        zh (ndarray): shape of (nMS x nME,1).
            vector of values for the variable Z corresponding to the s/t points ch
    
    Examples:
        >>> from stamps.general.valstvgx import valstg2stv

        Assuming there are two measuring sites which has observed data in time [2019, 2020].
        >>> cMS = np.array([[50,50],[-50,-50],[0,0]])
        >>> tME = np.array([2019,2020])
        >>> Z = np.array([[0.3,0.6],[-0.2,0.4],[0.13,0.3]])

        Coverting s/t grid format data to s/t vector format.
        >>> ch, zh = valstg2stv(Z, cMS, tME)

    '''
  
    nc=cMS.shape[0]
    nt=tME.size

    zh=(Z.T).reshape(nc*nt,1)
    ch=np.asarray(list(zip(np.tile(cMS[:,0],nt),np.tile(cMS[:,1],nt), \
                  tME.repeat(nc))))
                  
    return ch, zh

if __name__ == "__main__":
    import time

    data='../examples/Data/GeoData.xls'      
    datadf=pd.ExcelFile(data).parse('Sheet1',header=None)
    ch=datadf.iloc[:,0:3].values
    z=datadf.iloc[:,4].values.reshape(ch.shape[0],1)

    stime=time.time()
    Z1,cMS,tME,nanloc=valstv2stg(ch,z)
    print(time.time()-stime)

    ch2,z2=valstg2stv(Z1,cMS,tME)

    Z2,cMS2,tME2,nanloc=valstv2stg(ch2,z2,cMS[3:,:],tME[3:])
    Z3,cMS3,tME3,nanloc=valstv2stg(ch2,z2)

    id=np.where((Z1!=Z2))
    print(id)
